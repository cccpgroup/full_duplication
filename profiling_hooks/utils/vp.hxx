//this is for the value profiling
#ifndef VALUE_PROFILING_H
#define VALUE_PROFILING_H

#include <inttypes.h>
#include <iostream>
#include <ostream>

#include <map>
#include <vector>
#include <iterator>

using namespace std;

#define MAX_VALUES 9
#define HALFSIZE MAX_VALUES / 2

namespace VP {

  class ValueCount{
  public:
    int value;
    uint32_t count;
    ValueCount() : count(0) {}
  };

  typedef vector<ValueCount*> valueVec_t;

  class VProfile {
    private:
      //static const uint32_t MAX_VALUES = 9;
      static uint32_t CLR_INTERVAL;

      //keeps track of values to occurance counts
      valueVec_t valueToExeCount;
      //keeps track of execution count of a particular instruction
      uint64_t exeCount;

    public:
      VProfile() : exeCount(0) {
        for(uint32_t i = 0; i < MAX_VALUES; i++){
          valueToExeCount.push_back(new ValueCount());
        }
      }

      void increment(const int value);
      void keep_top_half();

      friend ostream &operator<<(ostream &stream, const VProfile &vp);
  };

  ostream &operator<<(ostream &stream, const VProfile &vp) {
    //stream<<vp.exeCount<<" : ";
    //if (vp.exeCount == 0)
    //  return stream;

    //for(uintmap_t::const_iterator I = vp.valueToExeCount.begin();
     //   I != vp.valueToExeCount.end(); I++){
     for(uint32_t i = 0; i < MAX_VALUES; i++){
      if((vp.valueToExeCount[i])->count != 0){
        stream << (vp.valueToExeCount[i])->value << " " << (vp.valueToExeCount[i])->count << " "; 
      }
      else{
        break;
      }
    }
    return stream;
  }
  uint32_t VProfile::CLR_INTERVAL = 2000;
}

#endif
