/*
 *	InstrRelInfo.cpp
 */

#define DEBUG_TYPE "InstrRelInfo"
#include "InstrRelInfo.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm/Support/CommandLine.h"
#include "llvm/Analysis/ProfileInfo.h"
#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include "llvm/IR/Instruction.h"
#include "llvm/IR/Instructions.h"
#include <string>
#include "llvm/IR/User.h"
#include "llvm/IR/BasicBlock.h"
#include "llvm/IR/Type.h"
#include "llvm/IR/CFG.h"
#include "llvm/Support/Debug.h"

using namespace llvm;

//constructor
InstrRelInfo::InstrRelInfo(Instruction *pInstr){
  uniqueID = 0;
  instr = pInstr;
  name = instr->getOpcodeName();
  opcode = instr->getOpcode();
  isSafe = false; 
  duplicated = false;
  duplicatedBy = NULL;
  isProtected = false;
  lifted = false;
  directSympGenDone = false;
  indirectSympGenDone = false;
  producerSetReady = false;
  consumerSetReady = false;
  debug1 =true;
  isSilentStore = false;
  loadAlreadyConsidered = false;
  producerChainLength = 0;
  visitedForLength = false;
  dynProdChainLength = 0;
  consideredForInvariance = false;
}

bool InstrRelInfo::printInstr(){
  DEBUG(dbgs() << "Griffin INSN: " << *instr << "\n");
  return true;
}

bool InstrRelInfo::isHighValue(){
  //TODO: currently only instructions which can
  //modify memory are considered high value instructions
  if(isSilentStore){
    DEBUG(dbgs() << "\n\n\n" << *instr << "\n\n\n\n");
  }
  //return((instr->mayWriteToMemory() || instr->isTerminator()) && (!isSilentStore));
  return(instr->mayWriteToMemory() && (!isSilentStore));
	/*return (instr->getDesc().mayStore() ||
          isCall(instr)||
          isFunctionArgument);
	*/
}

void InstrRelInfo::initStaticMembers() {
  consumerSet.clear();
  producerSet.clear();

  // populate symptom buckets
  sympBuckets.push_back(10);
  sympBuckets.push_back(100);
  sympBuckets.push_back(1000);
  sympBuckets.push_back(2000);
  sympBuckets.push_back(10000);

  // define symptom thresholds
  safeSympThreshold.first = 1000; //TODO: Originally 100, but we feel it should be 1000
  safeSympThreshold.second = RelSafeThreshold;
  liftSympThreshold.first = 10000;
  liftSympThreshold.second = RelLiftThreshold;
  indirectScalingFactor = RelIScale;
  indirectScalingThreshold = RelIScaleThreshold;
  directScalingThreshold = RelIScaleThreshold;
  
}

bool InstrRelInfo::accessesMem(Instruction* instr) {
  //return (instr->mayReadFromMemory() || instr->mayWriteToMemory());
  return (isa<StoreInst>(instr) || isa<LoadInst>(instr));
}

bool InstrRelInfo::accessesGlobalMem(Instruction* mi){
  bool isGlobal = false;
  for (unsigned int i = 0; i < mi->getNumOperands(); i++) {
    Value *mop= mi->getOperand(i);
    if(isa<GlobalValue>(mop)){
      isGlobal = true;
    }
  }
  return isGlobal;
}

bool InstrRelInfo::isGlobalStore(Instruction* mi){
  bool isGlobal = false;
  for (unsigned int i = 0; i < mi->getNumOperands(); i++) {
    Value *mop= mi->getOperand(i);
    if (isa<GlobalValue>(mop)){ // && isa<StoreInst>(mop)) {
      isGlobal = true;
    }
  }
  return (isGlobal && isa<StoreInst>(mi));//mi->mayWriteToMemory());
}	

bool InstrRelInfo::isCall(Instruction* instr){return isa<CallInst>(instr);}

/* MEMBER METHODS
 *
 * */

void InstrRelInfo::addUse(std::map<Instruction*, InstrRelInfo*> &iriMap, Instruction *pmi, unsigned int dist) {
  // avoid duplicates
  //   + can occur if the use instr has the same def as multiple
  //     operands
  //                    -OR-
  //   + make sure the "shorter" distance is retained for the use
  //   + check for illegal PHI cases
  
  if (pmi == instr) {
    DEBUG(dbgs() << "[addUse]: pmi == (instr)\n");
    if (!isa<PHINode>(pmi)) {	
      if(debug1) DEBUG(dbgs() <<  "[InstrRelInfo]: attempted to add a use=def! " << "DEF: " << *instr << "   USE: " << *pmi << "\n");	
    }
      DEBUG(dbgs() << "[InstrRelInfo]: ignoring def/use PHI\n");
      return;
  }

  DEBUG(dbgs() << "[addUse]: Attempting to add " << *pmi << "\n");
  UseInfo *ui;
  if (useBBs.find(pmi->getParent()) == useBBs.end()) {
    ui = new UseInfo();                                       
    DEBUG(dbgs() << "Creating new UseInfo for " << *pmi << "\n");
    useBBs[pmi->getParent()] = ui;
  } 
  else { 
    DEBUG(dbgs() << "Adding UseInfo for " << *pmi << "\n");
    ui = useBBs[pmi->getParent()]; 
  }
  if(ui->find(pmi) == ui->end()) {
    DEBUG(dbgs() << "[addUse]: Populating RelUseDetails struct for: " << *pmi << "\n");
    // populate RelUseDetails struct
    RelUseDetails *rud = new RelUseDetails;
    rud->dist = dist;
    rud->prob = -1;
    rud->iri = iriMap[pmi];
    (*ui)[pmi] = rud;
  } 
  else {
    if ((*ui)[pmi]->dist != dist) {
      DEBUG(dbgs() << "[InstrRelInfo addUse]: found \"duplicate\" uses w/ different distances for "
                 << *pmi);
      DEBUG(dbgs() << "[InstrRelInfo addUse]: orig instr " << *instr);
      DEBUG(dbgs() << dist << " " << (*ui)[pmi]->dist << "\n");
    }
  }
}

void InstrRelInfo::markSafe() {
  assert((directSympGenDone && indirectSympGenDone) &&
         "[InstrRelInfo]: attempted to make safe-ness determination without complete symptom map!\n");
  // make safe-ness determination
  //   + defaults to unsafe
  //   + a PHI node should never be "safe"
  //      + it should only be used to propagate def-use information
  if (isa<PHINode>(instr)){
    isSafe = false;
    if(debug1) DEBUG(dbgs() << "[InstrRelInfo]: PHI node encountered; always marked as unsafe\n");
  } 
  else {
    if(debug1){
      DEBUG(dbgs() << "\nmarkSafe: " << *instr << "\n");
      DEBUG(dbgs() << "sympMapBucketed[safeSympThreshold.first]" << sympMapBucketed[safeSympThreshold.first] << "\n");
      DEBUG(dbgs() << "safeSympThreshold.second: " << safeSympThreshold.second << "\n");
      }
    if (sympMapBucketed[safeSympThreshold.first] >= safeSympThreshold.second) {
      isSafe = true;
      #undef DEBUG_TYPE
      #define DEBUG_TYPE "SAFE"
      DEBUG(dbgs() << "SAFE: " << *instr << "\n");
      #undef DEBUG_TYPE
      #define DEBUG_TYPE "InstrRelInfo"
      dupChainSize = 0;
    } 
    else { isSafe = false; }
  }
  if(debug1){  DEBUG(dbgs() << "[InstrRelInfo]: " << "isMem? " << accessesMem(instr)
               << " isGlobalMem? " << accessesGlobalMem(instr)
               << " isaStore? " << isa<StoreInst>(instr)//->mayWriteToMemory()	//mayStore
               << " isaLoad? " << isa<LoadInst>(instr)//instr->mayReadFromMemory()	//mayLoad
               << " isSafe? " << isSafe << "\n\n\n");
  }
}

void InstrRelInfo::genUseList(std::map<Instruction*, InstrRelInfo*> &iriMap) {
#undef DEBUG_TYPE
#define DEBUG_TYPE "GenUses"
  
    		if (isa<CallInst>(instr)) {
          // function calls implicitly def "everything"
        }	
        else { 
            if(isa<StoreInst>(instr) || isa<BranchInst>(instr) || isa<ReturnInst>(instr) || isa<SelectInst>(instr)){  
              DEBUG(dbgs() << "Not pushing back: "<<*instr<<"\n");
            } else {
              if(debug1) DEBUG(dbgs() << "[genUseList]: Adding to defs: " << *instr /* *operand */ << "\n");
              defs.push_back(instr); 		
            }
    }

  // find all uses for each def
  for (std::vector<Instruction*>::iterator def_iter = defs.begin(); def_iter
      != defs.end(); def_iter++) { 
      Instruction* defInsn = *def_iter;
      // walk uses	
    
      if(debug1) DEBUG(dbgs() << "[genUseList]: Current defInsn we are iterating over: " << *defInsn << "\n");
       for(Value::use_iterator useIt = defInsn->use_begin(), useIt_e = defInsn->use_end();
          useIt != useIt_e; useIt++){ 
        Instruction *pUseInstr = dyn_cast<Instruction>(*useIt); 
        if(debug1)DEBUG(dbgs() << "[genUseList]: Found a use of defInsn(" << *defInsn << "): " << *pUseInstr <<"\n");
        unsigned int dist = 0;  
        if (pUseInstr->getParent() == instr->getParent()) {
          // use is in the same BB
          //  + find distance between def (instr) and use
          bool found_def = false;
          for (BasicBlock::iterator instr_iter = instr->getParent()->begin();
               instr_iter != instr->getParent()->end(); ++instr_iter) {
            Instruction *pCurInstr = &*instr_iter;
            if (found_def) {
              dist++;
              if (pCurInstr == pUseInstr) { break; } 
            }
            if (pCurInstr == instr) { found_def = true; }
          }
        } else {
          // use in a different BB
          //  + find distance to top of BB
          //   + find distance through all basic blocks between pCurInstr and pUseInst
          for (BasicBlock::iterator instr_iter = pUseInstr->getParent()->begin(); 
               instr_iter != pUseInstr->getParent()->end(); ++instr_iter) {
            Instruction *pCurInstr = &*instr_iter;	
            if (pCurInstr == pUseInstr) { break; }
              ++dist; 
          }
        }
        addUse(iriMap, pUseInstr, dist);
      }
  }
#undef DEBUG_TYPE
#define DEBUG_TYPE "InstrRelInfo"
}

void InstrRelInfo::processDirectConsumers(BBProfInfo &bbProf) {
  doProcessDirectConsumers(bbProf, instr->getParent(), 0, 1.0, 0);
  directSympGenDone = true;
}

void InstrRelInfo::processIndirectConsumers(BBProfInfo &bbProf) {
  if (indirectSympGenDone) { return; }
  indirectSympGenDone = true;
  // perform depth-first traversal of consumers and then process indirect consumers
  //    + this ensures that indirect consumer information is propagated up from
  //      all "leaf" nodes (nodes with no consumers)
  for (std::map<BasicBlock*, UseInfo*>::iterator usebb_iter = useBBs.begin();
       usebb_iter != useBBs.end(); ++usebb_iter) { 
    UseInfo* pUI = (*usebb_iter).second;
    for (UseInfo::iterator ui_iter = pUI->begin(); ui_iter != pUI->end(); ++ui_iter) {
      RelUseDetails* pRUD = (*ui_iter).second;
      pRUD->iri->processIndirectConsumers(bbProf);
    }
  }
  doProcessIndirectConsumers(bbProf, instr->getParent(), 0, 1.0);
}

void InstrRelInfo::doProcessDirectConsumers(BBProfInfo &bbProf,
                                            BasicBlock* pBB,
                                            unsigned int dist,
                                            double scale,
                                            int depth) {
  #undef DEBUG_TYPE
  #define DEBUG_TYPE "processDirect"
  if(defs.empty()) { 
    if(debug1)DEBUG(dbgs() << "\ndefs of "<<*instr<<" empty\n");
    return; 
  }
  if(debug1)DEBUG(dbgs() << "\n[doProcessDirectConsumers]: defs[0]: " << *defs[0]<< "\n");
  
  /**
   *  Account for symptom generating probabilities
   *  of direct consumers
   *    + walk CFG looking for uses in DFS fashion
   *        + scale by profiling probabilities
   *        + terminate DFS when:
   *            + scale < threshold
   *                  -OR-
   *            + dist > threshold
   *                  -OR-
   *            + encounter parent BB again (loop)
   *                  + loops that do not contain the parent BB are allowed
   **/
 

  bool zeroth = false;
  for (succ_iterator s_iter = succ_begin(pBB); s_iter != succ_end(pBB); ++s_iter) {
    //DEBUG(dbgs() << "Entering succ iterator loop\n");
    BasicBlock *pSBB;
    pSBB = *s_iter;
    int sizez;
    sizez = pBB->size();
    /*if(!zeroth){
        pSBB = pBB;	//If we're just entering this, process direct consumers in the current BB
	      zeroth = true;
        sizez = 0;
        DEBUG(dbgs() << "For debug purposes: pSBB != instr->getParent() --- " << (pSBB != instr->getParent()) << "\n");
    }*/
    double newScale = (*bbProf[pBB])[pSBB] * scale; 
    if (pSBB != instr->getParent() && newScale > directScalingThreshold && dist < 1000) {
        // make recursive call
        if(debug1)DEBUG(dbgs() << "[doProcessDirectConsumers]: Making direct consumer recursive call\n ");
        if(debug1)DEBUG(dbgs() << "[doProcessDirectConsumers]: dist, newScale: " << dist << " " << newScale << "\n");
        doProcessDirectConsumers(bbProf, pSBB, dist + sizez, newScale, ++depth);
    }
  }

  if (useBBs.find(pBB) != useBBs.end()) {
    if(debug1)	DEBUG(dbgs() << "[doProcessDirectConsumers]: Updating symptom map\n"); 
    for (UseInfo::iterator use_iter = useBBs[pBB]->begin(); use_iter != useBBs[pBB]->end(); ++use_iter) {  
      Instruction *pMI = (*use_iter).first;
      bool isAddress = false;
      Instruction *defInsn = dyn_cast<Instruction>(defs[0]);
      unsigned int distDelta = (*use_iter).second->dist;

      if(debug1) {
        DEBUG(dbgs() << "[doProcessDirectConsumers]:\n");
        DEBUG(dbgs() << "\tpBB: "<< pBB->getName() << "\nCurrent defs[0] Definition Instruction[?]: " << *defInsn << "\n");
        DEBUG(dbgs() << "\tCurrent Possible Use Being Analyzed for isAddress[?]: " << *pMI << "\n"); 
      }
    
      if(pMI->mayReadFromMemory() || pMI->mayWriteToMemory()){
        for(User::op_iterator itr =  pMI->op_begin(), itr_e = pMI->op_end(); itr != itr_e; ++itr){
          Value* usedVal = *itr;
          if(isa<GlobalValue>(usedVal)){
            //DEBUG(dbgs() << "global value: " << *usedVal << "\n");
          }
          else{
            Instruction *it = dyn_cast<Instruction>(usedVal);
            if ((defInsn == it) && !isa<CallInst>(pMI)){
              if (isa<StoreInst>(pMI)){
                if(defInsn == (dyn_cast<Instruction>(pMI->getOperand(1)))) {
                  if(isa<CallInst>(it) || isa<PHINode>(it)){
                    //isAddress = false;
                  }
                  else{
                    isAddress = true;
                    DEBUG(dbgs() << "FOO Address True: "<< *pMI <<"\n");
                  }
                } 
                else { 
                  isAddress =false; 
                }
              }  
              else{          
              /* if(isa<PHINode>(it)){
                  //isAddress = false;
                  DEBUG(dbgs() << "FOO CallInst or PHINode\n");
                }
                else{*/
                  isAddress = true;
                  DEBUG(dbgs() << "FOO Address True: "<< *pMI <<"\n");
                //} 
              }
            } else {} 
          }
        }
      }

    if (isAddress) { 
      directSympMap[dist + distDelta] += scale * 1;
      if(debug1){
        DEBUG(dbgs() << "isAddress true. dist, distDelta, scale: " << dist << "|" << distDelta << "|" << scale << "\n");
		    DEBUG(dbgs() << "isAddress instruction: " << *pMI << "\n");
        DEBUG(dbgs() << "directSympMap["<<(dist + distDelta)<<"]: " <<  directSympMap[dist + distDelta] << "\n");
      }
    }
    else{
      if(debug1) DEBUG(dbgs() << "isAddress false.  False instruction: " << *pMI << "\n");
    }
  }
  #undef DEBUG_TYPE
  #define DEBUG_TYPE "InstrRelInfo"
 }
}
void InstrRelInfo::doProcessIndirectConsumers(BBProfInfo &bbProf,
                                              BasicBlock* pBB,
                                              unsigned int dist,
                                              double scale) {
  /**
   *  Account for symptom generating probabilities
   *  of indirect consumers
   *    + walk CFG looking for uses in DFS fashion
   *        + scale by profiling probabilities
   *            + also scale by indirectScalingFactor
   *        + terminate DFS when:
   *            + scale < threshold
   *                  -OR-
   *            + dist > threshold
   *                  -OR-
   *            + encounter parent BB again (loop)
   *                  + loops that do not contain the parent BB are allowed
   **/
  
  bool zeroth = false;
  for (succ_iterator s_iter = succ_begin(pBB); s_iter != succ_end(pBB); ++s_iter) {
    BasicBlock *pSBB;
    pSBB = *s_iter;
    int sizez;
    sizez = pBB->size();
    /*if(!zeroth){
        pSBB = pBB;        //If we're just entering this, process indirect consumers in the current BB
	      zeroth = true;
        sizez = 0;
    }*/
    double newScale = (*bbProf[pBB])[pSBB] * scale;
    if (pSBB != instr->getParent() && newScale > directScalingThreshold && dist < 1000) {
        // make recursive call
        doProcessIndirectConsumers(bbProf, pSBB, dist + sizez, newScale);
    }
  }

  // update symptom map
  if (useBBs.find(pBB) != useBBs.end()) {
    for (UseInfo::iterator use_iter = useBBs[pBB]->begin();
         use_iter != useBBs[pBB]->end(); ++use_iter) {  
      RelUseDetails *pRUD = (*use_iter).second;
      unsigned int distDelta = pRUD->dist;
      InstrRelInfo* pUseIRI = pRUD->iri;

  #undef DEBUG_TYPE
  #define DEBUG_TYPE "SAFE"
      // update symptom probability map with indirect consumers
      for (std::map<unsigned int, double>::iterator symp_iter = pUseIRI->directSympMap.begin();
           symp_iter != pUseIRI->directSympMap.end(); symp_iter++) {

        unsigned int newDist = dist + distDelta + (*symp_iter).first;
        double prob = (*symp_iter).second;
        DEBUG(dbgs() << "FOO: update symptom prob map w/ indirect consumers\n");
        DEBUG(dbgs() << "FOO newDist, prob, scale: " << newDist << "|" << prob << "|" << scale << "\n");  
        if (indirectSympMap.find(newDist) == indirectSympMap.end()) {
          indirectSympMap[newDist] = (prob * scale * indirectScalingFactor);
        } else {
          indirectSympMap[newDist] += (prob * scale * indirectScalingFactor);
        }
      }
    #undef DEBUG_TYPE
    #define DEBUG_TYPE "InstrRelInfo"

    }
  }
}

void InstrRelInfo::populateSympBuckets() {
  DEBUG(dbgs() << "Direct: \n");
  doPopulateSympBuckets(directSympMap);
  DEBUG(dbgs() << "Indirect: \n");
  doPopulateSympBuckets(indirectSympMap);
}

void InstrRelInfo::doPopulateSympBuckets(std::map<unsigned int, double> &sympMap) {
    #undef DEBUG_TYPE
    #define DEBUG_TYPE "BUCKETS"
  // populate symptom buckets
  for (std::map<unsigned int, double>::iterator prob_iter =
       sympMap.begin(); prob_iter != sympMap.end(); prob_iter++) {
    unsigned int dist = (*prob_iter).first;
    double prob = (*prob_iter).second;
    DEBUG(dbgs() << "FOO: Populate Symp Buckets\n");
    DEBUG(dbgs() << "FOO dist, prob: " << dist << "|" << prob << "\n");
    bool foundBucket = false;
    for (std::list<unsigned int>::iterator it = sympBuckets.begin(); 
         it != sympBuckets.end(); it++) {
      if (dist < *it) {
        // found bucket
        sympMapBucketed[*it] += prob;
        DEBUG(dbgs() << "sympMapBucketed["<<*it<<"] += "<<prob<<" is "<<sympMapBucketed[*it]<<"\n");
        foundBucket = true;
      }
    }
    if (!foundBucket) {
      DEBUG(dbgs()
          << "[InstrRelInfo]: error! Symptom buckets need to be adjusted to include "
          << dist << "\n");
      exit(-1);
    }
  }
    #undef DEBUG_TYPE
    #define DEBUG_TYPE "InstrRelInfo"

}

std::string InstrRelInfo::printSymptomProb() {
  std::stringstream oss;
  oss << "(" << sympMapBucketed[safeSympThreshold.first] << ","
      << sympMapBucketed[liftSympThreshold.first] << ")";
  return oss.str();
}

std::string InstrRelInfo::printSymptomMap() {
  std::stringstream oss;
  oss << "\tdirect symptom map:\n";
  for (std::map<unsigned int, double>::iterator prob_iter = directSympMap.begin();
       prob_iter != directSympMap.end(); prob_iter++) {
    oss << (*prob_iter).first << "->" << (*prob_iter).second << " ";
  }
  oss << "\n";

  oss << "\tindirect symptom map:\n";
  for (std::map<unsigned int, double>::iterator prob_iter = indirectSympMap.begin();
       prob_iter != indirectSympMap.end(); prob_iter++) {
    oss << (*prob_iter).first << "->" << (*prob_iter).second << " ";
  }
  oss << "\n";

  oss << "\tbucketed symptom map:\n";
  for (std::map<unsigned int, double>::iterator prob_iter =
      sympMapBucketed.begin(); prob_iter != sympMapBucketed.end(); prob_iter++) {
    oss << (*prob_iter).first << "->" << (*prob_iter).second << " ";
  }
  return oss.str();
}

instrInfo::instrInfo(Instruction* cInstr){
  instr = cInstr;
  isDuplicated = false;
  isSafe = false;
  duplicatedBy = NULL;
}
bool instrInfo::isHighValue(){
  //return true if it's a call or invoke instruction.
  bool retVal = false;
  if(instr->getOpcode() == Instruction::Call ||
     instr->getOpcode() == Instruction::Invoke){
    retVal = true;
  }
  return retVal;
}

// static definitions
std::list<unsigned int> InstrRelInfo::sympBuckets;
std::set<Instruction*> InstrRelInfo::consumerSet;
std::set<Instruction*> InstrRelInfo::producerSet;
std::pair<unsigned int, double> InstrRelInfo::safeSympThreshold;
std::pair<unsigned int, double> InstrRelInfo::liftSympThreshold;
double InstrRelInfo::indirectScalingFactor;
double InstrRelInfo::indirectScalingThreshold;
double InstrRelInfo::directScalingThreshold;
