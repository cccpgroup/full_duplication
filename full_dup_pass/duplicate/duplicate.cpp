
#define DEBUG_TYPE "RelCodeGen"
#include "llvm/Pass.h"
#include "llvm/IR/Module.h"
#include "llvm/IR/Function.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm/Analysis/ProfileInfo.h"
#include "llvm/IR/Intrinsics.h"
#include "llvm/Analysis/LoopPass.h"
#include "llvm/Analysis/Passes.h"
//for some statistic variables
#include "llvm/ADT/Statistic.h"
//for debug information
#include "llvm/Support/Debug.h"
//for commandline options
#include "llvm/Support/CommandLine.h"
#include "llvm/IR/CFG.h"
#include "llvm/IR/InstIterator.h"
//The followign is utility to treat call/invoke as same
#include "llvm/IR/CallSite.h"
#include "llvm/IR/Instructions.h"
#include "llvm/Transforms/Utils/BasicBlockUtils.h"
#include "llvm/Analysis/PostDominators.h"
#include "llvm/IR/IRBuilder.h"
#include "llvm/IR/LLVMContext.h"
#include "LAMP/LAMPLoadProfile.h"
#include "VP/VPLoadProfile.h"
#include "InstrRelInfo.h"
#include "globalStores.h"
#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <string>
#include <vector>
#include <cmath>
using namespace llvm;

STATISTIC(totSafeInstr, "The # of safe instructions");
STATISTIC(totPInstr, "The # of instructions");
STATISTIC(totHVInstr, "The # of high value instructions");
//All of the following are dynamic counts based on dynamic profiling
//STATISTIC(totPInstrDyn, "The # of dynamic instructions");
//STATISTIC(totHVInstrDyn, "The # of dynamic high value instructions");
//STATISTIC(totStDyn, "The total number # of dynamic stores");
//STATISTIC(totSilentStDyn, "The total number # of dynamic silent stores");

//options
cl::opt<bool> RelPrintProfile("print-profile-info",
                            cl::desc("RelCodeGen print profile info"),
                            cl::init(false));
cl::opt<bool> localStats("localstats",
                            cl::desc("print the statistic collected"),
                            cl::init(false));
cl::opt<bool> RelDupAll("rcg-dup-all",
                            cl::desc("Duplicate all code"),
                            cl::init(true));
cl::opt<bool> RelDupCtrl("rcg-dup-ctrl",
                            cl::desc("RelCodeGen protect control"),
                            cl::init(true));
cl::opt<bool> RelUseProfile("rcg-use-profile",
                            cl::desc("RelCodeGen enable use of profile info"),
                            cl::init(false));
cl::opt<bool> RelUseLAMP("rcg-use-lamp",
                            cl::desc("RelCodeGen enable use of lamp"),
                            cl::init(false));
cl::opt<bool> RelUseVP("rcg-use-vp",
                            cl::desc("RelCodeGen enable use of value profling"),
                            cl::init(false));
cl::opt<bool> RelBB("rcg-insert-BB",
                            cl::desc("RelCodeGen insert BB for transfer of control"),
                            cl::init(true));
cl::opt<int> RelCtrlLevel("rcg-control-level",
                            cl::desc("RelCodeGen specifiy the level of control flow to be protected"),
                            cl::init(1));
cl::opt<float> RelLiftThreshold("rcg-lift-threshold",
                            cl::desc("RelCodeGen probability threshold for lifting instructions"),
                            cl::init(100.0));
cl::opt<float> RelSafeThreshold("rcg-safe-threshold",
                            cl::desc("RelCodeGen probability threshold for \"safe\" instructions"),
                            cl::init(.001));   //Check this  
cl::opt<float> RelIScale("rcg-iscale",
                            cl::desc("RelCodeGen heuristic scaling factor for indirect consumers"),
                            cl::init(.9));  //Check this?
cl::opt<float> RelIScaleThreshold("rcg-iscale-threshold",
                            cl::desc("RelCodeGen heuristic scaling threshold for indirect consumers"),
                            cl::init(.35));   //Would tweaking this do something useful?  
cl::opt<double> RelsilentThr("rcg-silent-threshold",
                            cl::desc("RelCodeGen threshold for silent stores"),
                            cl::init(0.70)); 
cl::opt<double> RelvalueThr("rcg-value-threshold",
                            cl::desc("RelCodeGen threshold for considering a value as frequently generated"),
                            cl::init(0.99)); 
cl::opt<bool> RelInsertValueCmp("rcg-insert-value-cmp",
                            cl::desc("RelCodeGen insert the comparision for frequenlty occuring values."),
                            cl::init(false)); 
cl::opt<int> RelOptThr("rcg-opt-thr",
                            cl::desc("RelCodeGen threshold for breaking recursive duplication chain"),
                            cl::init(100)); 
cl::opt<bool> RelTraceThroughMem("rcg-trace-through-mem",
                            cl::desc("RelCodeGen trace the dependences through memory at every load"),
                            cl::init(false)); 
cl::opt<bool> RelBreakRec("rcg-break-rec",
                            cl::desc("RelCodeGen break recursion using edge profiling"),
                            cl::init(false)); 
cl::opt<bool> RelRemoveSilent("rcg-rm-silent",
                            cl::desc("RelCodeGen remove the silent stores"),
                            cl::init(false)); 
//The following input parameters are for duplicating the instructions using 
//-true-dup pass (TD: True Duplication)
cl::opt<bool> TDUseLAMP("td-use-lamp",
                            cl::desc("True Dup enable use of lamp"),
                            cl::init(false));
cl::opt<bool> TDUseProfile("td-use-profile",
                            cl::desc("True Dup enable use of profile info"),
                            cl::init(false));


//STATISTIC(BBCounter, "Counts number of basic blocks");
namespace {
  typedef std::map<std::pair<Instruction*, Instruction*>*, unsigned int> DepToTimesMap_t;
  typedef std::pair<Instruction*, Instruction*> DepPair_t;
  struct RelCodeGen: public FunctionPass {
    static char ID; // Pass identification, replacement for typeid
    RelCodeGen() : FunctionPass(ID) {}

    /* global variables */
    // debug triggers //
    static bool debug1;
    // stat counters //
    static unsigned long instrID;
   // static unsigned long totLiftedInstr;
   // static unsigned long totDupInstr;
    static unsigned long totSafeInstr;
    static unsigned long totInstr;
    static unsigned long totHVCallsDyn;
    static unsigned long totHVOthers;
    static unsigned long totStDyn;
    static unsigned long totSilentStDyn;
  //  static unsigned long totHighValueInstr;
  //  static unsigned long totInstrProcessed;
  //  static unsigned long totFuncCalls;
  //  static unsigned long totCheckWFlags;
  //  static unsigned long totCheckWOFlags;
  //  static unsigned long dupChainSize;
  //  static unsigned long relFoobar;
    static unsigned long long totPInstrDyn;
    static unsigned long long totHVInstrDyn;
    static long long valueProfileSavings;
    static long long invariantValues; //only values which fall on some producer chain
    static unsigned long long totalInvariantValues;
    static unsigned long long dynDupCount;


    /* local (machine-function) level variables */
    // stat counters
    unsigned long numLiftedInstr;
    unsigned long numDupInstr;
    unsigned long numSafeInstr;
    unsigned long numHighValueInstr;

    // profile pass typedefs
    typedef std::map<BasicBlock*, double> ExecProb;
    typedef std::map<BasicBlock*, ExecProb*> BBProfInfo;
    typedef std::map<Instruction*, InstrRelInfo*> tInstrToInfoObj;
    typedef std::map<const BasicBlock*, bool> ctrlDupType;

    //class variables
    Function* currF;
    BasicBlock* relExitBB;
    static Value *exitStr;

    ctrlDupType ctrlDuplicated;

    //########## bookkeeping datastructures  ##############
    tInstrToInfoObj instrToInfoObj;
    std::set<Instruction*> worklist;
    std::set<Instruction*> cmpInstInserted;
    std::set<Instruction*> vcmpInstInserted;
    std::list<Instruction*> allInstr; 
    std::map<Instruction*, InstrRelInfo*> instrVisited; 
    //std::set<CallSite*> globalStores;

    //profile information 
    ProfileInfo *PI;
    LAMPLoadProfile *LAMP;
    globalStores *GLBST;
    VPLoadProfile *VP;
    BBProfInfo bbProf;

    //dominator information
    PostDominanceFrontier *PDF; //post dominance frontier
    //dependences through memory
    std::set<CallSite*> libCalls;

    //########## class methods ##############
    //
    virtual void getAnalysisUsage(AnalysisUsage &AU) const {
      //AU.setPreservesAll();
      if(RelUseProfile){
        AU.addRequired<ProfileInfo>();
      }
      if(RelDupCtrl){
        AU.addRequired<PostDominanceFrontier>();
      }
      if(RelUseLAMP){
        AU.addRequired<LAMPLoadProfile>();
        AU.addRequired<globalStores>();
      }
      if(RelUseVP){
        AU.addRequired<VPLoadProfile>();
      }
    }
    virtual bool doFinalization(Module &M);
    int calculateProdChainLength(Instruction *pInst, int &recDepth, unsigned long &exeCount);
    //given the number of frequently values generated by an instruction 
    //and number of instruction which would be replicated by traversing 
    //producerchain it will return if inserting a comparision with 
    //the frequented value is beneficial or not.
    bool isBeneficial(int freqValues, int prodChain, Instruction* cInst);
    //Returns the frequent value produced by given instruction.
    int getFreqValue(Instruction* cInst, bool &exists);
    bool insertFreqValueCmp(Instruction* cInst);
    void insertValueMsg();
    bool isValueSilent(Instruction* inst);
    bool breakRecDuplication(Instruction* cInst, Instruction* opnd);
    int duplicateThroughMem();
    Instruction* duplicateThroughMemRec(Instruction* pInst, const std::string &chainPrefix);
    std::set<Instruction*> getDependentStores(Instruction* ldInst);
    std::string getSpaces(int spaceCount);
    //appends a comment safe in bc, the input should be a safe instruction.
    void annotateSafe(Instruction* pInst);
    void annotateDuplicated(Instruction* pInst);
    void annotateDuplicate(Instruction* pInst);
    void annotateHV(Instruction* pInst);
    virtual bool runOnFunction(Function &F) {
      currF = &F;
      //The following is just a debug code if we want to run it only
      //for a particular function
      //std::string funName (currF->getName());
      //std::string tmpFunName ("Option");
      //if(funName.compare(tmpFunName) !=  0)
      //return false;
      if (RelUseLAMP){
        LAMP = &getAnalysis<LAMPLoadProfile>();
        GLBST = &getAnalysis<globalStores>();
      }
      if(RelUseVP){
        VP = &getAnalysis<VPLoadProfile>();
      }

      if(RelDupCtrl){
        PDF = &getAnalysis<PostDominanceFrontier>();
      }

      //FIXME
      //reset variables
      //  -be sure to free the memory from 
      //   relinfo objects
      for(tInstrToInfoObj::iterator iter = instrToInfoObj.begin();
          iter != instrToInfoObj.end(); iter++){
          delete ((*iter).second);
      }

      //+ be sure to free memory from pointers
      for (std::map<Instruction*, InstrRelInfo*>::iterator iter = instrVisited.begin();
        iter != instrVisited.end(); iter++) {
        delete ((*iter).second);
      }


      worklist.clear();
      allInstr.clear();   
      instrVisited.clear();
      bbProf.clear();
      instrToInfoObj.clear();
      cmpInstInserted.clear();
      vcmpInstInserted.clear();
      ctrlDuplicated.clear();
      libCalls.clear();

      InstrRelInfo::initStaticMembers();

      if (RelUseProfile){
        PI = &getAnalysis<ProfileInfo>();
        if(RelPrintProfile){
          PI->dump(&F, false);
        }
      }

      /*MPDT = &getAnalysis<MachinePostDominatorTree>();
      if (RelDupCtrl) {
        MPDT->getBase().recalculate(MF);
        llvm::cerr << *MPDT;
      }*/


      DEBUG(dbgs() << "**************************" << "\n" );
      DEBUG(dbgs() << "[RelCodeGen]: processing function " << F.getName() 
                   << " (" << F.size() << " bbs, "
                   << "execCnt " << getExecutionCount(&F) 
                   << ")\n" );
      DEBUG(dbgs() << "**************************" << "\n" );


      //iterate over all instructions in current function

      for(inst_iterator I = inst_begin(F), E = inst_end(F); I != E; ++I){
        Instruction *pInst = &*I; //This converts iterator into instruction pointer
        InstrRelInfo* infoObj = new InstrRelInfo(pInst);
        //DEBUG(dbgs() << "pInst: " << *pInst << "\n");
        //DEBUG(dbgs() << "infoObj->isntr: " << *(infoObj->instr) << "\n");
        instrToInfoObj[pInst] = infoObj;
        worklist.insert(pInst);

        //call sites
        if(CallInst* callInst = dyn_cast<CallInst>(pInst)){
          if(debug1) DEBUG(dbgs() << "Call site: " << infoObj->printInstr() << "\n");
        }

#undef DEBUG_TYPE
#define DEBUG_TYPE "CallSite"
        // call/invoke sites
        switch(pInst->getOpcode()){
          case Instruction::Call:
          case Instruction::Invoke:{
            CallSite CS(pInst);
            //CS.getCalledValue()->dump();
            //iterate over arguments
            CallSite::arg_iterator B = CS.arg_begin(), E = CS.arg_end();
            for(CallSite::arg_iterator A = B; A != E; ++A){
              Value *val = A->get();
              if(isa<Constant>(val) || isa<Argument>(val) || isa<GlobalValue>(val)){
                if(debug1) DEBUG(dbgs() << " constant or arg or globalVal: " << val->getName() << "\n");
              }
              else{
                //it's an instruction otherwise 
               if(debug1) DEBUG(dbgs() << *A->get() << "\n");
              }
              //DEBUG(dbgs() << " Daya " << "\n");
            }
            break;
          }
          default:
            break;
        }
      }

#undef DEBUG_TYPE
#define DEBUG_TYPE "RelCodeGen"

     /**
     * FIRST PASS
     *  + walks machine BBs
     *      + generates profile-based probabilities for BBs
     *      + identifies function calls and corresponding input arguments
     *  + walks instructions
     *      + creates InstrRelInfo object for each instruction
     *      + populates use list for each instruction
     **/

  DEBUG(dbgs() << "********* START PASS 1 *********" << "\n" );
      //walk basic block
      for(Function::iterator bb_iter = F.begin(); bb_iter != F.end(); ++bb_iter){
        BasicBlock* bb = &(*bb_iter);
        DEBUG(dbgs() << "--------------------------" << "\n" );
        DEBUG(dbgs() << "[RelCodeGen]: found BB " << bb->getName() 
                     << "(execCnt " << getExecutionCount(bb) << ")"
                     << "(size " << bb->size() << ")"
                    // << *bb
                     << "\n" );
        DEBUG(dbgs() << "--------------------------" << "\n" );

        processBB(bb);
  
      // walk instructions
      for (BasicBlock::iterator instr_iter = bb->begin(); instr_iter
          != bb->end(); ++instr_iter) {
          Instruction * pInstr = &(*instr_iter);
          if(RelUseProfile){
            totPInstrDyn += PI->getExecutionCount(pInstr->getParent());
          }

          allInstr.push_back(pInstr);

          //if(currF->getName() == "main"){
          //pInstr->dump();
          //errs() << "inserting above\n";
          //}
          // create new InstrRelInfo for current instruction
          InstrRelInfo* pIRI = new InstrRelInfo(pInstr);
          instrVisited[pInstr] = pIRI;    
          pIRI->uniqueID = instrID++;
          //dependences through memory
          //identify the lib function calls.
#undef DEBUG_TYPE
#define DEBUG_TYPE "libCalls"
          switch(pInstr->getOpcode()){
            case Instruction::Call:
            case Instruction::Invoke:{
              CallSite* CS = new CallSite(pInstr);
              Function* libCall = CS->getCalledFunction();
              //if(libCall && libCall->getName() == "printf"){
              //if(libCall && libCall->isDeclaration()){
              if(libCall){
                libCalls.insert(CS);
                DEBUG(dbgs() << "libcall: " << *pInstr << "\n");
              }
              break;
            }
            default:
              break;
          }
#undef DEBUG_TYPE
#define DEBUG_TYPE "RelCodeGen"
      }
      
      // walk instructions again and mark instructions that are
      // input arguments to function calls
          //TODO: processFunctionCalls(bb);
      }

      //#if 0
      // walk all instructions
      for (std::list<Instruction*>::iterator instr_iter = allInstr.begin();
          instr_iter != allInstr.end(); ++instr_iter) {
          Instruction* pInstr = *instr_iter;
          DEBUG(dbgs() << "\n[InstrRelInfo]: Instruction being processed: " << *pInstr << "\n");
          processInstr(pInstr);
          totInstr = totInstr+1;
      }
      DEBUG(dbgs() << "***********  END PASS 1  *************\n");

    /**
     * SECOND PASS
     *  + walks all instructions (IRI objects)
     *      + populates direct consumer map
     **/
    DEBUG(dbgs() << "\n***********  START PASS 2 ***********\n");
    for (std::list<Instruction*>::iterator instr_iter = allInstr.begin();
         instr_iter != allInstr.end(); instr_iter++) {
      Instruction* pMI = *instr_iter;
      InstrRelInfo* pIRI = instrVisited.find(pMI)->second;

      // populate symptom generating probability map for direct consumers
      DEBUG(dbgs() << "[InstrRelInfo]: Processing direct consumers\n");
      if(debug1)DEBUG(dbgs() << "pIRI: " << (instrVisited.find(pMI)->second) << "\n");
      pIRI->processDirectConsumers(bbProf);

    }
    DEBUG(dbgs() << "\n***********  END PASS 2  *************\n\n");

    /**
     * THIRD PASS
     *  + walks all instructions (IRI objects)
     *      + populates indirect consumer map
     *      + marks safe instructions
     *      + annotates stats (for output in assembly file)
     **/
    DEBUG(dbgs() << "***********  START PASS 3 ***********\n");
    for (std::list<Instruction*>::iterator instr_iter = allInstr.begin();
             instr_iter != allInstr.end(); instr_iter++) {
      Instruction* pMI = *instr_iter;
      InstrRelInfo* pIRI = instrVisited.find(pMI)->second;
      if(debug1) DEBUG(dbgs() << "[RelCodeGen Pass 3]: " << *(pIRI->instr) << "\n");

      // populate symptom generating probability map for indirect consumers
      DEBUG(dbgs() << "[InstrRelInfo]: Processing indirect consumers\n");
      pIRI->processIndirectConsumers(bbProf);

      // populate symptom probability buckets
      pIRI->populateSympBuckets();  
      if (debug1) DEBUG(dbgs() << "[RelCodeGen]: printSymptomMap:" << pIRI->printSymptomMap() << "\n");

      // identify safe instructions
      if (!RelDupAll) {
        pIRI->markSafe(); //DEBUG
        if (pIRI->isSafe) {
          //DEBUG(dbgs() << "RELCODEGEN SAFE: " << *(pIRI->instr) << "\n");
	        DEBUG(dbgs() << "SAFE INSTRUCTION COUNT: " << totSafeInstr+1 << "\n\n\n");
	        totSafeInstr++; 
        }
      }
    }

    // populate consumerSet
    DEBUG(dbgs() << "[relCodeGen]: Populating consumer Set\n");
    std::set<Instruction*>* pVisited = new std::set<Instruction*>();
    for (std::list<Instruction*>::iterator instr_iter = allInstr.begin();
         instr_iter != allInstr.end(); instr_iter++) {
         Instruction* pMI = *instr_iter;
      updateConsumerSet(pVisited, pMI);
    }
    DEBUG(dbgs() << "\n***********  END PASS 3  *************\n");
    //#endif

    //duplicate the producers, final transformation phase
    DEBUG(dbgs() << "\n***********  START PASS 4  *************\n");
    if(RelRemoveSilent){
      removeSilentFromHighValue(); 
    }
    if(RelTraceThroughMem){
      duplicateThroughMem();
    }
    else{
      duplicateProducers();
    }
    //split the BB with comparisions and point to relExitBB.
    createRelExitBB();
    splitBBAndPointExitBlock();
    splitAtInsert();
    insertValueMsg();


    
    DEBUG(dbgs() << "\n***********  END PASS 4  *************\n");
    #undef DEBUG_TYPE
    #define DEBUG_TYPE "SAFE"
    DEBUG(dbgs() << "Number of safe instructions: " << totSafeInstr << "\n");
    DEBUG(dbgs() << "Total number of instructions: " << totInstr << "\n");
    #undef DEBUG_TYPE
    #define DEBUG_TYPE "RelCodeGen"
    return true;
    }
   
  InstrRelInfo* processInstr(Instruction *instr) {
    InstrRelInfo* pIRI = instrVisited[instr]; 
    DEBUG(dbgs() << "[RelCodeGen]: processing instruction " << pIRI->uniqueID
                 << " (" << pIRI << ") " << *pIRI->instr << "\n");

      // populate use list
    if(debug1)DEBUG(dbgs() << "[InstrRelInfo]: Generating and Populating Use List\n");  
    pIRI->genUseList(instrVisited);

   if (debug1) {
      DEBUG(dbgs() << "\t[RelCodeGen] Defs of " << *pIRI->instr << ": ");
      for (std::vector<Instruction*>::iterator operand_iter = pIRI->defs.begin();
          operand_iter != pIRI->defs.end(); operand_iter++) {
        DEBUG(dbgs() << **operand_iter << ", ");
      }
      DEBUG(dbgs() << "\n\t[RelCodeGen] Uses of " << *pIRI->instr << ": ");
      for (std::map<BasicBlock*, InstrRelInfo::UseInfo*>::iterator useBB_iter = pIRI->useBBs.begin();
           useBB_iter != pIRI->useBBs.end(); ++useBB_iter) {
        for (InstrRelInfo::UseInfo::iterator use_iter = (*useBB_iter).second->begin();
             use_iter != (*useBB_iter).second->end(); ++use_iter) {
          DEBUG(dbgs() << "\t\t(" << (*use_iter).second->iri << ") " << *((*use_iter).first));
        }
      }
      DEBUG(dbgs() << "\n");
    }
    return pIRI;
  }

  void annotateStats(InstrRelInfo &iri) {
    std::stringstream oss;
    oss << "STATS:" << " ID=" << iri.uniqueID
        << " HV=" << iri.isHighValue()
        << " SAFE=" << iri.isSafe;
    appendComments(iri.instr, oss);
  }

  void appendComments(Instruction* pMI, std::stringstream &oss) {
   /* if (pMI->relComment.empty()) {	
      pMI->relComment += "# RELCODEGEN";
    }
    pMI->relComment += " (";
    pMI->relComment += oss.str();
    pMI->relComment += ")";
   */
  }

    void processBB(BasicBlock *pBB){
    //The profiling information is already available from 
    //ProfileInfo. I am not sure if we need the following information
    //as of yet. For getting an edge weight, the following can be used
    //ProfileInfo::Edge e = ProfileInfo::getEdge(pBB, pCurBB);
    //PI->getEdgeWeight(e)

      assert((bbProf.find(pBB) == bbProf.end()) &&
           "[RelCodeGen]: attempted to process BB multiple times!\n");
      double tot = 0;
      ExecProb *pEP = new ExecProb;
     // DEBUG(dbgs() << "[processBB]: " << pBB->getName() << " => " << getExecutionCount(pBB) << "\n");
      for(succ_iterator s_iter = succ_begin(pBB); s_iter != succ_end(pBB); ++s_iter){
        BasicBlock *pCurBB = *s_iter;
        double execCnt = getExecutionCount(pCurBB);
        ProfileInfo::Edge e = ProfileInfo::getEdge(pBB, pCurBB);
        if(execCnt == ProfileInfo::MissingValue){
          execCnt = 0;
        }
        if(debug1)DEBUG(dbgs() << "[processBB]: Successor:  " << pCurBB->getName() << " => " << execCnt << "\n");
        if(debug1) DEBUG(dbgs() << PI->getEdgeWeight(e) << "\n");
        (*pEP)[pCurBB] = execCnt;
        tot += execCnt;
      }
      if(debug1)DEBUG(dbgs() << "[processBB]: Total Execution Count: " << pBB->getName() << " " << tot << "\n");
      for(succ_iterator s_iter = succ_begin(pBB); s_iter != succ_end(pBB); ++s_iter){
        BasicBlock *pCurBB = *s_iter;
        if((*pEP)[pCurBB] != 0){
          (*pEP)[pCurBB] = (*pEP)[pCurBB]/tot;
        }
      }

  bbProf[pBB] = pEP;

#undef DEBUG_TYPE
#define DEBUG_TYPE "processBB"
    }
    //get the execution count from profiling information for a 
    //function
    double getExecutionCount(Function* pF){
      if(RelUseProfile){
        return PI->getExecutionCount(pF);
      }
      else{
        //default
        return -1.0;
      }
    }

    //get the execution count from profiling information for a 
    // basic block
    double getExecutionCount(BasicBlock* pB){
      if(RelUseProfile){
        return PI->getExecutionCount(pB);
      }
      else{
        //default
        return 1.0;
      }
    }
    
  void updateConsumerSet(std::set<Instruction*>* pVisited, Instruction* pCurMI) {
    if (pVisited->find(pCurMI) != pVisited->end()) {
      return;
    }
    pVisited->insert(pCurMI);
    InstrRelInfo* pCurIRI = instrVisited.find(pCurMI)->second;
    if (!pCurIRI->consumerSetReady) {
      for (std::map<BasicBlock*, InstrRelInfo::UseInfo*>::iterator ui_iter = pCurIRI->useBBs.begin();
           ui_iter != pCurIRI->useBBs.end(); ++ui_iter) {
        InstrRelInfo::UseInfo* pUI = (*ui_iter).second;
        for (InstrRelInfo::UseInfo::iterator rud_iter = pUI->begin(); rud_iter != pUI->end(); ++rud_iter) {
          Instruction* pConsumer = (*rud_iter).first;
          InstrRelInfo* pConsumerIRI = instrVisited.find(pConsumer)->second;
          if (!pConsumerIRI->consumerSetReady) {
            updateConsumerSet(pVisited, pConsumer);
          }
          pCurIRI->consumerSet.insert(pConsumer);
          pCurIRI->consumerSet.insert(pConsumerIRI->consumerSet.begin(), pConsumerIRI->consumerSet.end());
        }
      }
      pCurIRI->consumerSetReady = true;

      std::stringstream oss;
      oss << "CONSUMER SET";
      for (std::set<Instruction*>::iterator pmi_iter = pCurIRI->consumerSet.begin();
           pmi_iter != pCurIRI->consumerSet.end(); ++pmi_iter) {
        oss << " " << (instrVisited.find(*pmi_iter))->second->uniqueID;
      }
      appendComments(pCurMI, oss);
    }
  }

    //Starting at the operand of high-value instructions,
    //duplicate producers recursively
    int duplicateProducers(){
#undef DEBUG_TYPE
#define DEBUG_TYPE "DupProducers"
      int longestPath = 0;
      bool tmpExecuted = false;
      DEBUG(dbgs() << "** Duplicating producers of HV in function " <<  currF->getName() << "***" << "\n");
      int HV_chain = 0;
      std::stringstream fixChainName("HV");
      //for(tInstrToInfoObj::iterator iter = instrToInfoObj.begin();
      //  iter != instrToInfoObj.end(); ++iter){FIXME
      for (std::map<Instruction*, InstrRelInfo*>::iterator iter = instrVisited.begin();
        iter != instrVisited.end(); iter++) { 
          totPInstr++;
          if(RelUseProfile){
            //totPInstrDyn += PI->getExecutionCount(((*iter).first)->getParent());
            //DEBUG(dbgs() << "total:" << totPInstrDyn << "\n");
            if(isValueSilent((*iter).first)){
              totalInvariantValues += PI->getExecutionCount(((*iter).first)->getParent());
            }
          }
          if((iter->second)->isSafe){
            annotateSafe(iter->first);
          }
          //check for highvalue
        if((*iter).second->isHighValue()){

            //update statstic variables
            totHVInstr++;
            annotateHV((*iter).first);
            if(RelUseProfile){
              BasicBlock* parentBB = ((*iter).first)->getParent();
              unsigned long exeCount = PI->getExecutionCount(parentBB); 
              totHVInstrDyn += exeCount;
              DEBUG(dbgs() << "HV count:" << totHVInstrDyn << "\n");
              if(isa<CallInst>((*iter).first)){
                totHVCallsDyn += exeCount;
              }
              else if(isa<StoreInst>((*iter).first)) {
                //counted using totStDyn
                totStDyn += exeCount;
              }
              else{
                totHVOthers += exeCount;
              }
            }
            //if(HV_chain < 3){
            //HV_chain++;
            //continue;
            //}
            std::stringstream chainName;
            chainName << fixChainName.str() << HV_chain << "_";
            Instruction *currInst = (*iter).first;
            //if(CallInst* currCall = dyn_cast<CallInst>(currInst)){
            //DEBUG(dbgs() << "Call" << currCall->getCalledValue() << "\n");
            //}
            DEBUG(dbgs() << "High Val " << *currInst << "\n");
            //iterate over operands
            for(User::op_iterator itr =  currInst->op_begin(), 
                itr_e = currInst->op_end(); itr != itr_e; ++itr){
              Value* usedVal = *itr;
              //Value can be constant, argument, globalvalue or instructions
              //We do not duplicate constants, argument would be protected 
              //at call sites and globalvalue need not be protected as address 
              //of loads/stores are protectected.
              if(Instruction *usedInstr = dyn_cast<Instruction>(usedVal)){
                DEBUG(dbgs() << "daya " << *usedInstr << "\n");
                int recDepth = 0;
                unsigned long exeCount = 0;
                int recLength = calculateProdChainLength(usedInstr, recDepth, exeCount);
                if(!breakRecDuplication(currInst, usedInstr)){
                  //recursive call
                  Instruction* dupInst = duplicateProdRec(usedInstr, chainName.str());
       
                  insertCmpHV(currInst, dupInst, usedInstr, "HVCmp");
                }
                  /*
                  switch((usedInstr->getType())->getTypeID()){
                    //FIXME: Handle the floating type
                    case Type::IntegerTyID:
                    case Type::PointerTyID:{
                      DEBUG(dbgs() << "We insert interger cmp " << *usedInstr << "\n");
                      Instruction* cmpInst = new ICmpInst(currInst, ICmpInst::ICMP_NE, usedInstr, dupInst, "HVCmp");
                      break;
                    }
                    default:
                     errs() << "Warning: This type is not handled\n"; 
                  }
                  */
                tmpExecuted = true;
              }
            }

            //duplicateProdRec(currInst, chainName.str());
            HV_chain++;
            //if(tmpExecuted)
            //break;

            //duplicate the control flow for the given level
            if(RelDupCtrl){
              DEBUG(dbgs() << "Protecting control chain \n");
              chainName << "CT_";
              duplicateControl(currInst->getParent(), chainName.str());
            }

          }
      }
#undef DEBUG_TYPE
#define DEBUG_TYPE "RelCodeGen"
      return longestPath;
    }

    //recursively duplicate producers until one of the following is
    //encountered
    // 1. instructions is safe
    // 2. instructions is already duplicated
    //
    // Returns the duplicated instruction
    Instruction* duplicateProdRec(Instruction *pInst, const std::string &chainPrefix){
#undef DEBUG_TYPE
#define DEBUG_TYPE "DupProducers"
      //clone and insert if not already duplicated && (if
      //the instruction is not safe || dupall option is given)
      //if(!instrToInfoObj[pInst]->isHighValue()){
      Instruction* duplicatedInst = NULL;
      bool isProfitable = false;
      //FIXME
      if(instrVisited[pInst]->duplicated){
        duplicatedInst = instrVisited[pInst]->duplicatedBy;
      }
      else{
        if((!instrVisited[pInst]->isSafe) || RelDupAll){
          bool breakAtFreqValue = false;
          if(RelInsertValueCmp){
            //is it benificial to use value profile information instead of duplication?
            if(RelUseVP){
              if(isValueSilent(pInst)){
                if(!instrVisited[pInst]->consideredForInvariance){
                  instrVisited[pInst]->consideredForInvariance = true;
                  invariantValues += PI->getExecutionCount(pInst->getParent());
                  isProfitable = isBeneficial(1, instrVisited[pInst]->producerChainLength, pInst);
                  if(isProfitable){
                    //duplicatedInst = pInst;
                    insertFreqValueCmp(pInst);
                    breakAtFreqValue = true;
                    DEBUG(dbgs() << "beneficial for " << *pInst << "\n");
                  }
                }
              }
            }
          }
          if(breakAtFreqValue){
            //if we inserted a freq value comparision here then we don
            //t need to consider for further recursive duplication.
            DEBUG(dbgs() << "producer chain terminated. VP\n");
          }
          //duplicate load the address chain of a load and insert comparision
          else if(pInst->getOpcode() == Instruction::Load){
            DEBUG(dbgs() << "not duplicating load/store" << *pInst << "\n");
            Value* loadAddr = pInst->getOperand(0);
            if(Instruction* loadAddrInst = dyn_cast<Instruction>(loadAddr)){
              DEBUG(dbgs() << "address is " << *loadAddrInst << "\n");
              if(!breakRecDuplication(pInst, loadAddrInst)){
                Instruction* dupInst = duplicateProdRec(loadAddrInst, chainPrefix);
                insertCmpHV(pInst, dupInst, loadAddrInst, "LDCmp");
              }
            }
          }
          else if (pInst->getOpcode() == Instruction::Alloca || 
                   pInst->getOpcode() == Instruction::Call){
            //do not duplicate alloca instruction, as this allocated memory
            //do not duplicate call instruction, its operand would be taken
            //care of by HV inst
            DEBUG(dbgs() << "not duplicating alloca" << *pInst << "\n");
          }
          else{
            //overhead of duplicated instructions.
            if(RelUseProfile){
              dynDupCount += PI->getExecutionCount(pInst->getParent());
              annotateDuplicated(pInst);
            }
            Instruction *dupInst = pInst->clone();
            dupInst->setName(chainPrefix);
            //dupInst->setParent(pInst->getParent());
            //pInst->getParent()->getInstList().insert(pInst, dupInst);
            //std::vector<Value*> Args(5);
            LLVMContext &ctxt = currF->getContext();
            //const Type* intTp = Type::getInt1Ty(ctxt);
            //Args[0] = ConstantInt::get(intTp, 1);
            //Args[1] = ConstantInt::get(intTp, 1);
            //Args[2] = ConstantInt::get(intTp, 1);
            //Args[3] = ConstantInt::get(intTp, 1);
            //Args[4] = ConstantInt::get(intTp, 1);
            //Function *readIntrin = Intrinsic::getDeclaration(currF->getParent(), Intrinsic::memory_barrier);
            dupInst->insertAfter(pInst);
            //Instruction* readInst = CallInst::Create(readIntrin, Args.begin(), Args.end(), "", dupInst);
            instrVisited[pInst]->duplicated = true;
            instrVisited[pInst]->duplicatedBy = dupInst;
            duplicatedInst = dupInst;
            DEBUG(dbgs() << "parent: " << dupInst->getParent() << "\n");

            annotateDuplicate(dupInst);

            //iterate over operands
            unsigned oprndCount = 0;
            for(User::op_iterator itr =  pInst->op_begin(), 
                itr_e = pInst->op_end(); itr != itr_e; ++itr){
              Value* usedVal = *itr;
              if(Instruction *usedInstr = dyn_cast<Instruction>(usedVal)){
//                DEBUG(dbgs() << "daya " << *usedInstr << "\n");
                if(!breakRecDuplication(pInst, usedInstr)){
                  //recursive call
                  Instruction* dupUser = duplicateProdRec(usedInstr, chainPrefix);
                  if(duplicatedInst && dupUser){
                    duplicatedInst->setOperand(oprndCount, dupUser);
                  }
                }
              }
              oprndCount++;
            }
          }
        }
        //}
      }
      return duplicatedInst;
#undef DEBUG_TYPE
#define DEBUG_TYPE "RelCodeGen"
    }

#undef DEBUG_TYPE
#define DEBUG_TYPE "postDom"
    void duplicateControl(BasicBlock* pBB, const std::string &chainPrefix){
      /**
      * Duplicate producers for RelCtrlLevel level control (branches)
      * -> find control dependent bb
      *    -> X is control dependent on Y iff X does not post-dominate Y AND
      *       there exists a successor to Y, Y', post-dominated by X.
      */
      int level = 0;
      //get the frontier of the given basic block
      //PDF->dump();
      if(PDF->find(pBB) != PDF->end()){
        const DominanceFrontier::DomSetType &bbSet = PDF->find(pBB)->second;
        //iterate and print the name of the basic blocks
        for(DominanceFrontier::DomSetType::const_iterator I = bbSet.begin(),
              E = bbSet.end(); I != E; ++I){
          if(*I){    
            const BasicBlock* pFrontBB = *I;
            DEBUG(dbgs() << "Frontier BB of " << pBB->getName() << " is " << pFrontBB->getName() << "\n");
            duplicateControlRec(pFrontBB, level, chainPrefix);
          }
        }      
      }
    }
    void duplicateControlRec(const BasicBlock* pBB, int level, const std::string &chainPrefix){
      if(ctrlDuplicated.find(pBB) != ctrlDuplicated.end()){
        return;
      }
      else if(level < RelCtrlLevel){
        //duplicate the input chain of cmp operand of the pBB
        DEBUG(dbgs() << "Duplicating cmp of " << pBB->getName() << " in " << currF->getName() <<"\n"); 
        Instruction* termInst = (Instruction*)pBB->getTerminator();
        DEBUG(dbgs() << "terminator " << *termInst << "\n");
        //iterate over operands
        for(User::op_iterator itr =  termInst->op_begin(), 
            itr_e = termInst->op_end(); itr != itr_e; ++itr){
          Value* usedVal = *itr;
          if(Instruction *usedInstr = dyn_cast<Instruction>(usedVal)){
            if(!breakRecDuplication(termInst, usedInstr)){
              DEBUG(dbgs() << "daya " << *usedInstr << "\n");
              Instruction* dupInst = NULL;
              if(RelTraceThroughMem){
                dupInst = duplicateThroughMemRec(usedInstr, chainPrefix);
              }
              else{
                dupInst = duplicateProdRec(usedInstr, chainPrefix);
              }
              //insert comparisions if required
              insertCmpHV(termInst, dupInst, usedInstr, "CTCmp");
            }
          }  
        }    
        ctrlDuplicated[pBB] = true;
        //increase the recursion level and duplicate control flow recursively.
        level++;
        const DominanceFrontier::DomSetType &bbSet = PDF->find((BasicBlock*)pBB)->second;
        for(DominanceFrontier::DomSetType::const_iterator I = bbSet.begin(),
              E = bbSet.end(); I != E; ++I){
          if(*I){    
            const BasicBlock* pFrontBB = *I;
            duplicateControlRec(pFrontBB, level, chainPrefix);
          }
        }  
      }
      else{
        return;
      }
    }
#undef DEBUG_TYPE
#define DEBUG_TYPE "RelCodeGen"
    void insertCmpHV(Instruction* insertPt, Instruction* dupInst, Instruction* origInst, 
                            const std::string &instName){
      //if we duplicated the instruction
      //create a comparision between duplicated operand and original instruction.
      //e.g. 
      //
      //%2 = getelementptr inbounds [100 x i32]* %a, i64 0, i64 %1
      //store i32 0, i32* %2, align 4
      //===>
      //%HV3_ = getelementptr inbounds [100 x i32]* %a, i64 0, i64 %HV3_2
      //%2 = getelementptr inbounds [100 x i32]* %a, i64 0, i64 %1
      //<Comparision here>
      //store i32 0, i32* %2, align 4
      //for the mini example, %HV3_ is dupInst and origInst is %2
      //FIXME: Are we inserting duplicate comparisions???
      if(dupInst){
        Instruction* cmpInst = NULL;
        //adjust dynDupCount
        //we are inserting two extra (cmp and br) for every high value comparision.
        //dynDupCount += 2*(PI->getExecutionCount(origInst->getParent()));
        if((origInst->getType())->isIntOrIntVectorTy() || 
            (origInst->getType())->isPointerTy()){
          cmpInst = new ICmpInst(insertPt, ICmpInst::ICMP_NE, origInst, dupInst, instName); 
          cmpInstInserted.insert(cmpInst);
        }
        else if((origInst->getType())->isFPOrFPVectorTy()){
          cmpInst = new FCmpInst(insertPt, FCmpInst::FCMP_ONE, origInst, dupInst, instName); 
          cmpInstInserted.insert(cmpInst);
        }
        else{
          errs() << "Warning: This type is not handled\n"; 
        }
      }  
    }

#undef DEBUG_TYPE
#define DEBUG_TYPE "splitBB"
    void splitBBAndPointExitBlock(){
      for(std::set<Instruction*>::iterator I = cmpInstInserted.begin(),
            E = cmpInstInserted.end(); I != E; ++I){
        //if the RelBB option is given split the basic block to
        //use the result of cmp instruction to divert control flow
        //relExit BB.
        Instruction* cmpInst = *I;
        if(RelBB){
          BasicBlock::iterator splitIt = cmpInst;
          splitIt++;
          BasicBlock* oldBB = cmpInst->getParent();
          BasicBlock* newBB = SplitBlock(oldBB, splitIt, this);
          Instruction* termInst = oldBB->getTerminator();
          //FIXME: condBr is never used?
          BranchInst* condBr = BranchInst::Create(relExitBB, newBB, cmpInst, oldBB);
          //remove the original uncond branch
          termInst->eraseFromParent();
        }
      }      
      return;
    }
    void createRelExitBB(){
      //create an exit basic block
      //this basic block would be transferred control if 
      //any of the checkers fail.
      if(RelBB){
        relExitBB = BasicBlock::Create(currF->getContext(), "relExit", currF);
        Value* retVal = NULL;
        //Constant* funcName = ConstantArray::get(F.getContext(), "myFunc");
        IRBuilder<> builder(relExitBB);
        //exitString << "Reliability CMP failed in function \%s" << currF->getNameStr() << "\n";
        if(!exitStr){
          std::stringstream exitString;
          exitString << "Reliability CMP failed in function %s" << "\n";
          exitStr = builder.CreateGlobalStringPtr(exitString.str().c_str(), "relFun");
        }
        Value* funcName = builder.CreateGlobalStringPtr(currF->getName(), "relFun");

        std::vector<Type*> params;
        Type* ret_t; //FIXME: Never used
        FunctionType* ft;
        LLVMContext &ctxt = currF->getContext();
        Type* intTp = Type::getInt32Ty(ctxt);
        Type* voidTp = Type::getVoidTy(ctxt);
        Type* ptr8Type = Type::getInt8PtrTy(ctxt);
        Module *M = currF->getParent();

        //insert the printf call
        params.push_back(ptr8Type);
        ft  = FunctionType::get(intTp, params, true);
        Constant* printFun = M->getOrInsertFunction("printf", ft);
        builder.CreateCall2((Value*)printFun, exitStr, funcName);
        //F.getReturnType()->dump();
        //insert the exit function
        params.clear();
        params.push_back(Type::getInt32Ty(currF->getContext()));
        ft  = FunctionType::get(voidTp, params, false);
        Value *constOne = ConstantInt::get(intTp, 1);
        Constant *exitFunc = M->getOrInsertFunction("exit", ft);
        //insert the exit function call
        builder.CreateCall((Value*)exitFunc, constOne);

        //This doesn't seem to be right way to do it: FIXME
        //What shoud be the terminator of block containing 
        //exit instruction?
        if(!(currF->getReturnType()->isVoidTy())){
          retVal = Constant::getNullValue(currF->getReturnType());
        }

        //FIXME: Never used
        ReturnInst* retInst = ReturnInst::Create(currF->getContext(), retVal, relExitBB);
        /*
        switch(F.getReturnType()->getTypeID()){
          //FIXME: This doesn't handle all the cases.
          case Type::VoidTyID:{
            ReturnInst* retInst = ReturnInst::Create(F.getContext(), NULL, relExitBB);
          }
          case Type::IntegerTyID:{
            Value* retVal = ConstantInt::get(F.getReturnType(), 0);
            ReturnInst* retInst = ReturnInst::Create(F.getContext(), retVal, relExitBB);
          }
          case default:
            errs() << "Warning: This type is not handled\n"; 
        }*/
      }


      return;
    }
#undef DEBUG_TYPE
#define DEBUG_TYPE "splitAtInsert"
    void splitAtInsert(){
      if(RelBB){
        for(std::map<Instruction*, InstrRelInfo*>::iterator iter = instrVisited.begin();
          iter != instrVisited.end(); ++iter){
          Instruction* splitAt = (*iter).first;
          if(instrVisited[splitAt]->duplicated){
            BasicBlock::iterator splitIt = splitAt;
            splitIt++;
            BasicBlock* oldBB = splitAt->getParent();
            DEBUG(dbgs() << "splitting ..." << "\n");
            DEBUG(dbgs() << oldBB << "\n");
            BasicBlock* newBB = SplitBlock(oldBB, splitIt, this);
            //Instruction* termInst = oldBB->getTerminator();
            //BranchInst* uncondBr = BranchInst::Create(newBB);
            //remove the original branch
            //termInst->eraseFromParent();
          }
        }
      }
      return;
    }
#undef DEBUG_TYPE
#define DEBUG_TYPE "silent"
    //FIXME: This should be called once per module not on every function
    void removeSilentFromHighValue(){
      static int firstCall = 0;
      static std::ofstream resultOut;
      if(!firstCall)
        resultOut.open("silent_count_relPass.csv");
      if(RelUseLAMP && RelUseProfile){
        for(Function::iterator IF = currF->begin(), IE = currF->end();
            IF != IE; ++IF){
          BasicBlock&BB = *IF;
          for(BasicBlock::iterator I =BB.begin(), E = BB.end(); I != E; ++I){
            Instruction* pInst = &*I;
            if(isa<StoreInst>(pInst)){
              unsigned int totalExecs = PI->getExecutionCount(pInst->getParent());
              unsigned int instID = LAMP->InstToIdMap[pInst];
              unsigned int silentCount = LAMP->idToSilentCount[instID];
              double fractSilent = (double)silentCount/totalExecs;
              resultOut << instID << ", " << totalExecs << ", " << silentCount << "\n";
              //totStDyn += totalExecs;
              totSilentStDyn += silentCount;
              if(fractSilent > RelsilentThr){
                DEBUG(dbgs() << "marking silent " << instID << " fract: "  << fractSilent << "\n");
                instrVisited[pInst]->isSilentStore = true;
                DEBUG(dbgs()<< "The above instr would be removed from HV\n");
              }
            }
          }
        }
        /*
        std::map<unsigned int, unsigned int>::iterator iter;
        for(iter = LAMP->idToSilentCount.begin(); iter != LAMP->idToSilentCount.end(); iter++){
          unsigned int instID = iter->first;
          unsigned int silentCount = iter->second;
          Instruction* slStoreInst = LAMP->IdToInstMap[instID];
          //DEBUG(dbgs() << instID << " -> " << silentCount << "\n");
          //DEBUG(dbgs() << "instruction is: " << *(slStoreInst) << "\n");
          //get the execution count from profile
          //int totalExecs = PI->getExecutionCount(slStoreInst);
          unsigned int totalExecs = PI->getExecutionCount(slStoreInst->getParent());
          double fractSilent = (double)silentCount/totalExecs;
          if(!firstCall){
            resultOut << instID << ", " << totalExecs << ", " << silentCount << "\n";
            totStDyn += totalExecs;
            totSilentStDyn += silentCount;
          }
          DEBUG(dbgs() << "silent fraction " << fractSilent << "\n");
          //DEBUG(dbgs() << "total count " << totalExecs << "\n");
          if(fractSilent > RelsilentThr){
            if(instrVisited.find(slStoreInst) != instrVisited.end()){
              DEBUG(dbgs() << "marking silent " << instID << " fract: "  << fractSilent << "\n");
              instrVisited[slStoreInst]->isSilentStore = true;
              DEBUG(dbgs() << "The above instr would be removed from HV\n");
            }
          }

        }*/
      }
      firstCall = 1;
      return;
    }
#undef DEBUG_TYPE
#define DEBUG_TYPE "RelCodeGen"
 
  };
// static definitions
unsigned long RelCodeGen::instrID = 0;
bool RelCodeGen::debug1 = false;
//unsigned long RelCodeGen::totLiftedInstr = 0;
//unsigned long RelCodeGen::totDupInstr = 0;
unsigned long RelCodeGen::totSafeInstr = 0;
unsigned long RelCodeGen::totInstr = 0;
unsigned long RelCodeGen::totHVCallsDyn = 0;
unsigned long RelCodeGen::totHVOthers = 0;
unsigned long RelCodeGen::totStDyn = 0;
unsigned long RelCodeGen::totSilentStDyn = 0;
//unsigned long RelCodeGen::totHighValueInstr = 0;
//unsigned long RelCodeGen::totInstrProcessed = 0;
//unsigned long RelCodeGen::totFuncCalls = 0;
//unsigned long RelCodeGen::totCheckWFlags= 0;
//unsigned long RelCodeGen::totCheckWOFlags= 0;
//unsigned long RelCodeGen::dupChainSize = 0;
//unsigned long RelCodeGen::relFoobar = 0;
unsigned long long RelCodeGen::totPInstrDyn = 0;
unsigned long long RelCodeGen::totHVInstrDyn = 0;
long long RelCodeGen::valueProfileSavings = 0;
long long RelCodeGen::invariantValues = 0;
unsigned long long RelCodeGen::totalInvariantValues = 0;
unsigned long long RelCodeGen::dynDupCount = 0;
}

char RelCodeGen::ID = 0;
Value* RelCodeGen::exitStr = NULL;
static RegisterPass<RelCodeGen> X("duplicate", "Insert the reliability code");

bool RelCodeGen::doFinalization(Module &M){
  if(localStats){
    errs() << totPInstrDyn << " total instructions dynamic" << "\n";
    errs() << totHVInstrDyn << " total HV instructions dynamic" << "\n";
    errs() << totStDyn << " total store instructions dynamic" << "\n";
    errs() << totSilentStDyn << " total silent stores dynamic" << "\n";
    errs() << totHVCallsDyn << " total HV calls instructions dynamic" << "\n";
    errs() << totHVOthers << " total HV others instructions dynamic" << "\n";
    errs() << dynDupCount << " Extra dynamic instructions because of duplication " << "\n";
    errs() << "----------------------------------" << "\n";
    errs() << valueProfileSavings << " Savings if we use value profiling " << "\n";
    errs() << invariantValues << " invariant values (which are on producer chain) at threshold " << RelvalueThr  << "\n";
    errs() << totalInvariantValues << " all invariant values" << RelvalueThr  << "\n";
  }
  return true;
}
#undef DEBUG_TYPE
#define DEBUG_TYPE "AnnSafe"
void RelCodeGen::annotateSafe(Instruction* pInst){
  DEBUG(dbgs() << "safe inst" << *pInst << "\n");
  LLVMContext &ctxt = currF->getContext();
  unsigned mdKind = ctxt.getMDKindID("safe");
  llvm::Type *i32 = llvm::Type::getInt32Ty(ctxt);
  std::vector<Value*> Args(1);
  Constant *V = ConstantInt::get(i32, 1);
  Metadata *MDs[] = {ConstantAsMetadata::get(V)};
  //Args[0] = V;
  MDNode* n = MDNode::get(ctxt, MDs);
  pInst->setMetadata(mdKind, n);
}
void RelCodeGen::annotateDuplicate(Instruction* pInst){
  DEBUG(dbgs() << "Duplicated inst" << *pInst << "\n");
  LLVMContext &ctxt = currF->getContext();
  unsigned mdKind = ctxt.getMDKindID("dup");
  llvm::Type *i32 = llvm::Type::getInt32Ty(ctxt);
  std::vector<Value*> Args(1);
  Constant *V = ConstantInt::get(i32, 1);
  Metadata *MDs[] = {ConstantAsMetadata::get(V)};
  //Args[0] = V;
  MDNode* n = MDNode::get(ctxt, MDs);
  pInst->setMetadata(mdKind, n);
}
void RelCodeGen::annotateDuplicated(Instruction* pInst){
  DEBUG(dbgs() << "Duplicated inst" << *pInst << "\n");
  LLVMContext &ctxt = currF->getContext();
  unsigned mdKind = ctxt.getMDKindID("dupd");
  llvm::Type *i32 = llvm::Type::getInt32Ty(ctxt);
  std::vector<Value*> Args(1);
  Constant *V = ConstantInt::get(i32, 1);
  Metadata *MDs[] = {ConstantAsMetadata::get(V)};
  //Args[0] = V;
  MDNode* n = MDNode::get(ctxt, MDs);
  pInst->setMetadata(mdKind, n);
}
void RelCodeGen::annotateHV(Instruction* pInst){
  DEBUG(dbgs() << "High Value inst" << *pInst << "\n");
  LLVMContext &ctxt = currF->getContext();
  unsigned mdKind = ctxt.getMDKindID("hv");
  llvm::Type *i32 = llvm::Type::getInt32Ty(ctxt);
  std::vector<Value*> Args(1);
  Constant *V = ConstantInt::get(i32, 1);
  Metadata *MDs[] = {ConstantAsMetadata::get(V)};
  //Args[0] = V;
  MDNode* n = MDNode::get(ctxt, MDs);
  pInst->setMetadata(mdKind, n);
}
#undef DEBUG_TYPE
#define DEBUG_TYPE "dupThroughMem"
//Duplicate based on tracing the dependences through memory.
int RelCodeGen::duplicateThroughMem(){
  int HV_chain = 0;
  std::stringstream fixChainName("TM");//through memory
  if(RelUseLAMP){
    std::set<Instruction *>::iterator storeIter;
    for(storeIter = GLBST->globalStrs.begin(); storeIter != GLBST->globalStrs.end(); ++storeIter){
      Instruction* storeInst = *storeIter;
      if((storeInst->getParent())->getParent() == currF){
        DEBUG(dbgs() << "global store in " << (currF)->getName());
        DEBUG(dbgs() << *storeInst << "\n");
        std::stringstream chainName;
        chainName << fixChainName.str() << HV_chain << "_";
        for(User::op_iterator itr =  storeInst->op_begin(),
          itr_e = storeInst->op_end(); itr != itr_e; ++itr){
           Value* usedVal = *itr;
           if(Instruction *usedInstr = dyn_cast<Instruction>(usedVal)){
             int recDepth = 0;
             unsigned long exeCount = 0;
             int recLength = calculateProdChainLength(usedInstr, recDepth, exeCount);
             if(!breakRecDuplication(storeInst, usedInstr)){
               Instruction* dupInst = duplicateThroughMemRec(usedInstr, chainName.str());
               insertCmpHV(storeInst, dupInst, usedInstr, "HVCmp");
             }
           }
        }
        if(RelDupCtrl){
          DEBUG(dbgs() << "Protecting control chain \n");
          chainName << "CT_";
          duplicateControl(storeInst->getParent(), chainName.str());
        }
      }
      HV_chain++;
    }
  }
  DEBUG(dbgs() << "Number of lib calls:" << libCalls.size() << "\n");
  std::set<CallSite *>::iterator libcall;
  for(libcall = libCalls.begin(); libcall != libCalls.end(); ++libcall){
    CallSite* CS = *libcall;
    std::stringstream chainName;
    chainName << fixChainName.str() << HV_chain << "_";
    Instruction* currInst = CS->getInstruction();
    //iterate over arguments
    CallSite::arg_iterator B = CS->arg_begin(), E = CS->arg_end();
    for(CallSite::arg_iterator A = B; A != E; ++A){
      Value *val = A->get();
      if(isa<Constant>(val) || isa<Argument>(val) || isa<GlobalValue>(val)){
        DEBUG(dbgs() << " constant or arg or globalVal: " << val->getName() << "\n");
      }
      else{
        //it's an instruction otherwise 
        if(Instruction *usedInstr = dyn_cast<Instruction>(val)){
          int recDepth = 0;
          unsigned long exeCount = 0;
          int recLength = calculateProdChainLength(usedInstr, recDepth, exeCount);
          if(!breakRecDuplication(currInst, usedInstr)){
            Instruction* dupInst = duplicateThroughMemRec(usedInstr, chainName.str());
            insertCmpHV(currInst, dupInst, usedInstr, "HVCmp");
          }
        }
        //DEBUG(dbgs() << *A->get() << "\n");
      }
    
    }
    HV_chain++;

    //control flow protection for library calls.
    if(RelDupCtrl){
      DEBUG(dbgs() << "Protecting control chain \n");
      chainName << "CT_";
      duplicateControl(currInst->getParent(), chainName.str());
    }
  }

}
Instruction* RelCodeGen::duplicateThroughMemRec(Instruction* pInst, const std::string &chainPrefix){
  Instruction* duplicatedInst = NULL;
  if(instrVisited[pInst]->duplicated){
    duplicatedInst = instrVisited[pInst]->duplicatedBy;
  }
  else{
    if((!instrVisited[pInst]->isSafe) || RelDupAll){
      bool breakAtFreqValue = false;
      if(RelInsertValueCmp){
        //is it benificial to use value profile information instead of duplication?
        if(RelUseVP){
          if(isValueSilent(pInst)){
            if(!instrVisited[pInst]->consideredForInvariance){
              instrVisited[pInst]->consideredForInvariance = true;
              invariantValues += PI->getExecutionCount(pInst->getParent());
              bool isProfitable = isBeneficial(1, instrVisited[pInst]->producerChainLength, pInst);
              if(isProfitable){
                //duplicatedInst = pInst;
                insertFreqValueCmp(pInst);
                breakAtFreqValue = true;
                DEBUG(dbgs() << "beneficial for " << *pInst << "\n");
              }
            }
          }
        }
      }
      if(breakAtFreqValue){
        //if we inserted a freq value comparision here then we don
        //t need to consider for further recursive duplication.
        DEBUG(dbgs() << "producer chain terminated. VP\n");
      }
      else if(pInst->getOpcode() == Instruction::Load){ 
        if(!(instrVisited[pInst]->loadAlreadyConsidered)){
          //encounter a load
          instrVisited[pInst]->loadAlreadyConsidered = true;
          DEBUG(dbgs() << "load" << *pInst << "\n");
          DEBUG(dbgs() << "dependent on" << "\n");
          std::set<Instruction*> depStores = getDependentStores(pInst);
          std::set<Instruction *>::iterator storeIter;
          for(storeIter = depStores.begin(); storeIter != depStores.end(); ++storeIter){
            Instruction* storeInst = *storeIter;
            if(!instrVisited[storeInst]->isSilentStore){
              DEBUG(dbgs() <<  *storeInst << " \n");
              Value* storeData = storeInst->getOperand(0);
              DEBUG(dbgs() <<  "data operand " <<  *storeData << " \n");
              if(Instruction* storeDataInst = dyn_cast<Instruction>(storeData)){
                if(instrVisited.find(storeDataInst) != instrVisited.end()){
                  if(!instrVisited[storeDataInst]->duplicated){
                    if(!breakRecDuplication(storeInst,storeDataInst)){
                      Instruction* dupInst = duplicateThroughMemRec(storeDataInst, chainPrefix);
                      insertCmpHV(storeInst, dupInst, storeDataInst, "LDCmp");
                    }
                  }
                }
                else{
                  errs() << "processing " << (currF)->getName() << "\n";
                  storeDataInst->dump();
                  errs() << "Above instruction doesn't exist in instrVisited\n";
                }
              }
              if(RelDupCtrl){
                DEBUG(dbgs() << "Protecting control chain \n");
                std::stringstream chainName;
                chainName << "CT_";
                duplicateControl(storeInst->getParent(), chainName.str());
              }
            }
          }
        }
      }
      else if (pInst->getOpcode() == Instruction::Alloca || 
               pInst->getOpcode() == Instruction::Call){
      }
      else{
        if(RelUseProfile){
          dynDupCount += PI->getExecutionCount(pInst->getParent());
          annotateDuplicated(pInst);
        }
        instrVisited[pInst]->duplicated = true;
        //clone here
        Instruction *dupInst = pInst->clone();
        dupInst->setName(chainPrefix);
        LLVMContext &ctxt = currF->getContext();
        dupInst->insertAfter(pInst);
        instrVisited[pInst]->duplicated = true;
        instrVisited[pInst]->duplicatedBy = dupInst;
        duplicatedInst = dupInst;
        unsigned oprndCount = 0;
        for(User::op_iterator itr =  pInst->op_begin(), 
              itr_e = pInst->op_end(); itr != itr_e; ++itr){
          Value* usedVal = *itr;
          if(Instruction *usedInstr = dyn_cast<Instruction>(usedVal)){

            if(!breakRecDuplication(pInst, usedInstr)){
              Instruction* dupUser = duplicateThroughMemRec(usedInstr, chainPrefix);
              if(duplicatedInst && dupUser){
                duplicatedInst->setOperand(oprndCount, dupUser);
              }
            }
          }
          oprndCount++;
        }
      }
    }
  }
  return duplicatedInst;
}

std::set<Instruction*> RelCodeGen::getDependentStores(Instruction* ldInst){
    std::set<Instruction*> worklist;
    if(isa<LoadInst>(ldInst) && RelUseLAMP){
      DepToTimesMap_t::iterator iter;
      double ldExeCount = PI->getExecutionCount(ldInst->getParent());
      for(iter = LAMP->DepToTimesMap.begin(); iter != LAMP->DepToTimesMap.end(); iter++){
        DepPair_t *ldStPair = iter->first;
        unsigned int times = iter->second;
        unsigned int id1 = LAMP->InstToIdMap[ldStPair->first];
        unsigned int id2 = LAMP->InstToIdMap[ldStPair->second];
        if((ldInst == ldStPair->first) && (ldExeCount > 0.0)){
          //only the stores which are in current function should be returned.
          //FIXME: How should the dependent stores in other functions should be handled.
          if((ldInst->getParent())->getParent() == ((ldStPair->second)->getParent())->getParent()){
            //DEBUG(dbgs() << *ldStPair->second << " for " << times << " times\n");
            //DEBUG(dbgs() << *ldStPair->second << " for " << (double)times/ldExeCount << " times\n");
            worklist.insert(ldStPair->second);
          }
        }
      }
    }

  return worklist;
}
#undef DEBUG_TYPE
#define DEBUG_TYPE "prodChainLength"
//This function is supposed to behave in a way similar to duplicateProdRec.
//The purpose of this function is to calculate the producer chain length for a 
//specified High-Value instruction.
int RelCodeGen::calculateProdChainLength(Instruction *pInst, int &recDepth, unsigned long &exeCount){
  int chainLength = 0;
  if(!RelUseProfile){
    return chainLength;
  }
  recDepth++;
  DEBUG(dbgs() << getSpaces(recDepth) << "Current Inst:" << *pInst << " exe:" << 
                (unsigned long) PI->getExecutionCount(pInst->getParent())<< "\n");
  if(instrVisited[pInst]->visitedForLength){
    //duplicatedInst = instrVisited[pInst]->duplicatedBy;
    //chainLength = instrVisited[pInst]->producerChainLength;
  }
  else{
    if((!instrVisited[pInst]->isSafe) || RelDupAll){
      if(pInst->getOpcode() == Instruction::Load){
        Value* loadAddr = pInst->getOperand(0);
        if(Instruction* loadAddrInst = dyn_cast<Instruction>(loadAddr)){
          int recLength = calculateProdChainLength(loadAddrInst, recDepth, exeCount);
          if(recLength > chainLength){
            chainLength = recLength;
          }
        }
      }
      else if (pInst->getOpcode() == Instruction::Alloca || 
               pInst->getOpcode() == Instruction::Call){
        //return chainLength;
        //ignore these
        //exeCount = PI->getExecutionCount(pInst->getParent());
      }
      else{
        instrVisited[pInst]->visitedForLength = true;
        chainLength++;
        int recLength = 0;
        exeCount += PI->getExecutionCount(pInst->getParent());
        for(User::op_iterator itr =  pInst->op_begin(), 
            itr_e = pInst->op_end(); itr != itr_e; ++itr){
          Value* usedVal = *itr;
          if(Instruction *usedInstr = dyn_cast<Instruction>(usedVal)){
            unsigned long tmpExeCount = 0;
            recLength = calculateProdChainLength(usedInstr, recDepth, tmpExeCount);
            exeCount += tmpExeCount;
            chainLength += recLength;
          }
        }
      }
    }
  }
  if(chainLength > instrVisited[pInst]->producerChainLength){
    instrVisited[pInst]->producerChainLength = chainLength;
    //DEBUG(dbgs() << "producers chain length for " << *pInst << " :" << chainLength << "\n");
  }
  if(!instrVisited[pInst]->dynProdChainLength){
    instrVisited[pInst]->dynProdChainLength = exeCount;
  }
  DEBUG(dbgs() << getSpaces(recDepth) << *pInst << ":" << chainLength 
        << " exe:" << instrVisited[pInst]->dynProdChainLength << "\n");
  recDepth--;
  return chainLength;
}
std::string RelCodeGen::getSpaces(int spaceCount){
  std::stringstream spaces;
  for(int i = 0; i < spaceCount; i++){
    spaces << "    ";
  }
  return spaces.str();
}
bool RelCodeGen::isBeneficial(int freqValues, int prodChain, Instruction* cInst){
  bool isProfitable = false;
  //for n freq values the formula is 
  //prodChain > (n (cmps) + (log2)n (or tree) + 1 (br))
  int orTreeSize = std::ceil(std::log(freqValues));
  int extraInsts = freqValues + orTreeSize + 1;
  if(prodChain > extraInsts){
    isProfitable = true;
    if(RelUseProfile){
      valueProfileSavings += instrVisited[cInst]->dynProdChainLength;
      valueProfileSavings -= extraInsts*(PI->getExecutionCount(cInst->getParent()));
    }
    //valueProfileSavings += prodChain - (freqValues + orTreeSize + 1);
  }
  return isProfitable;
}
int RelCodeGen::getFreqValue(Instruction* cInst, bool &exists){
  int retVal = -1; 
  exists = false;
  if(VP->InstToIdMap.find(cInst) !=  VP->InstToIdMap.end()){
    unsigned int instID = VP->InstToIdMap[cInst];
    retVal = VP->IdToValueCount[instID]->begin()->first;
    exists = true;
  }
  return retVal;
}
bool RelCodeGen::insertFreqValueCmp(Instruction* cInst){
  Instruction* cmpInst = NULL;
  bool valueExists = false;
  int constVal = RelCodeGen::getFreqValue(cInst, valueExists);
  if(valueExists){
    //FIXME: Is the following condition correct? 
    //We are considering value profling only for integer values.
    if((cInst->getType())->isIntOrIntVectorTy()){ 
      LLVMContext &ctxt = currF->getContext();
      llvm::Type *instType = cInst->getType();
      Value  *V = ConstantInt::get(instType, constVal);
      std::string instName = "Vcmp";
      BasicBlock::iterator it(cInst);
      it++;
      cmpInst = new ICmpInst(&*it, ICmpInst::ICMP_NE, cInst, V, instName); 
      vcmpInstInserted.insert(cmpInst);
    }
    else{
      errs() << "Warning(VP): Type " << (cInst->getType())
              << " is not handled\n"; 
    }
  }
}
void RelCodeGen::insertValueMsg(){
  if(RelInsertValueCmp){
    Module *M = currF->getParent();
    for(std::set<Instruction*>::iterator I = vcmpInstInserted.begin(),
          E = vcmpInstInserted.end(); I != E; ++I){
       BasicBlock* valueMsgBB = BasicBlock::Create(currF->getContext(), "valMsg", currF);
       IRBuilder<> builder(valueMsgBB);
       LLVMContext &ctxt = currF->getContext();
       std::vector<Type*> params;
       Type* voidTp = Type::getVoidTy(ctxt);
       FunctionType* ft = FunctionType::get(voidTp, params, false);
       Constant *valMsgFun = M->getOrInsertFunction("printValueMsg", ft);
       builder.CreateCall((Value*)valMsgFun);
       Instruction* cmpInst = *I;
       BasicBlock::iterator splitIt = cmpInst;
       splitIt++;
       BasicBlock* oldBB = cmpInst->getParent();
       BasicBlock* newBB = SplitBlock(oldBB, splitIt, this);
       Instruction* termInst = oldBB->getTerminator();
       BranchInst* unCondBr = BranchInst::Create(newBB, valueMsgBB);
       BranchInst* condBr = BranchInst::Create(valueMsgBB, newBB, cmpInst, oldBB);
       //remove the original uncond branch
       termInst->eraseFromParent();
    }
  }      
  return;
}
bool RelCodeGen::isValueSilent(Instruction* inst){
  bool retVal = false;
  if(RelUseVP && RelUseProfile){
    if(VP->InstToIdMap.find(inst) !=  VP->InstToIdMap.end()){
      unsigned int instID = VP->InstToIdMap[inst];
      if(VP->IdToValueCount[instID]->size() > 0){
        unsigned int execCount = PI->getExecutionCount(inst->getParent());
        int value = VP->IdToValueCount[instID]->begin()->first;
        unsigned int count = VP->IdToValueCount[instID]->begin()->second;
        double fractionOfTotal = (double)count/execCount;
        if(fractionOfTotal >= RelvalueThr){
          retVal = true;
        }
      }
    }
  }
  return retVal;
}

#undef DEBUG_TYPE
#define DEBUG_TYPE "breakRec"
bool RelCodeGen::breakRecDuplication(Instruction* cInst, Instruction* opnd){
  bool retVal = false;
  if(RelUseProfile && RelBreakRec){
    double cExeCount = PI->getExecutionCount(cInst->getParent());
    double opExeCount = PI->getExecutionCount(opnd->getParent());
    double relExCount = opExeCount/cExeCount;
    if(relExCount > RelOptThr){
      retVal = true;
      DEBUG(dbgs() << "breaking recursion\n");
      DEBUG(dbgs() << "current Inst: " << *cInst << " exeCount: " << cExeCount << "\n");
      DEBUG(dbgs() << "opnd Inst: " << *opnd << " exeCount: " << opExeCount << "\n");
    }
  }
  return retVal;
}
#undef DEBUG_TYPE
#define DEBUG_TYPE "globalStores"
//This pass is to populate the list of the stores retruned by lamp 
//which are not currently in the function currently under processing.

char globalStores::ID = 0;
bool globalStores::flag = false;
static RegisterPass<globalStores>
H("global-stores", "creates a list of global stores.");

ModulePass *llvm::createglobalStoresPass(){
  return new globalStores();
}

bool globalStores::runOnModule(Module &M){
  if(flag == true)
    return false;
  if(RelUseProfile && RelUseLAMP){
    PI = &getAnalysis<ProfileInfo>();
    LAMP = &getAnalysis<LAMPLoadProfile>();
    DepToTimesMap_t::iterator iter;
    for(iter = LAMP->DepToTimesMap.begin(); iter != LAMP->DepToTimesMap.end(); iter++){
      DepPair_t *ldStPair = iter->first;
      unsigned int times = iter->second;
      Instruction* ldInst = ldStPair->first;
      Instruction* stInst = ldStPair->second;
      double ldExeCount = PI->getExecutionCount(ldInst->getParent());
      if(ldExeCount > 0.0){
        if(((ldInst->getParent())->getParent()) != ((stInst->getParent())->getParent())){
          globalStrs.insert(stInst); 
          DEBUG(dbgs() << "global store: " << *stInst << "\n");
        }
      }
    }
  }

  flag = true;
  return false;
}
#undef DEBUG_TYPE
#define DEBUG_TYPE "silentCount"
//This pass is to test and analyze the behavior of silent stores.
namespace {
  class silentCount : public ModulePass {
    public:
      static char ID;
      static bool flag;
      static char const * resultFile;
      ProfileInfo *PI;
      LAMPLoadProfile *LAMP;
      bool runOnModule(Module &M);
      virtual void getAnalysisUsage(AnalysisUsage &AU) const{
        AU.addRequired<LAMPLoadProfile>();
        AU.addRequired<ProfileInfo>();
      }
      silentCount(): ModulePass(ID){
      }
  };
}
char silentCount::ID = 0;
bool silentCount::flag = false;
char const * silentCount::resultFile = "silent_count.csv";

static RegisterPass<silentCount>
Z("silent-count", "silent store count debug and analysis.");

ModulePass *llvm::createsilentCount(){
  return new silentCount();
}

bool silentCount::runOnModule(Module &M){
  if(flag == true)
    return false;
  std::ofstream resultOut;
  resultOut.open(resultFile);
  PI = &getAnalysis<ProfileInfo>();
  LAMP = &getAnalysis<LAMPLoadProfile>();
    for(Module::iterator I = M.begin(), E = M.end(); I != E; ++I){
      if(!I->isDeclaration()){
        Function &F = *I;
        PI->dump(&F, false);
        for(inst_iterator Iter = inst_begin(F), End = inst_end(F); Iter != End; ++Iter){
          Instruction &pInst = *Iter;
          if(isa<StoreInst>(&pInst)){
            uint64_t exeCount = PI->getExecutionCount(pInst.getParent());
            unsigned int instID = LAMP->InstToIdMap[&pInst];
            unsigned int silentCount = LAMP->idToSilentCount[instID]; 
            //DEBUG(dbgs() << pInst << "\n");
            DEBUG(dbgs() << instID << ", " << exeCount << ", " << silentCount << "\n");
            resultOut << instID << ", " << exeCount << ", " << silentCount << "\n";
          }
        }
      }
    }
  flag = true;
  resultOut.close();
  return false;
}
#undef DEBUG_TYPE
#define DEBUG_TYPE "lampAnalysis"
//This pass is to test and analyze the dependence through memory.
namespace {
  typedef std::map<std::pair<Instruction*, Instruction*>*, unsigned int> DepToTimesMap_t;
  typedef std::map<BasicBlock*, std::set<std::pair<Instruction*, Instruction*>* > > LoopToDepSetMap_t;
  typedef std::pair<Instruction*, Instruction*> DepPair_t;
  class lampAnalysis : public ModulePass{
    public:
      static char ID;
      static bool flag;
      ProfileInfo *PI;
      LAMPLoadProfile *LAMP;
      bool runOnModule(Module &M);
      virtual void getAnalysisUsage(AnalysisUsage &AU) const{
        AU.addRequired<ProfileInfo>();
        AU.addRequired<LAMPLoadProfile>();
      }
      lampAnalysis(): ModulePass(ID){
      }
  };
}
char lampAnalysis::ID = 0;
bool lampAnalysis::flag = false;

static RegisterPass<lampAnalysis>
P("lamp-analysis", "test and analyze the dependence through memory");

ModulePass *llvm::createlampAnalysis(){
  return new lampAnalysis();
}

bool lampAnalysis::runOnModule(Module &M){
  if(flag == true)
    return false;
  PI = &getAnalysis<ProfileInfo>();
  LAMP = &getAnalysis<LAMPLoadProfile>();
  //DEBUG(dbgs() << "running on function: " << F.getName() << "\n");
  DepToTimesMap_t::iterator iter;
  for(iter = LAMP->DepToTimesMap.begin(); iter != LAMP->DepToTimesMap.end(); iter++){
    DepPair_t *ldStPair = iter->first;
    unsigned int times = iter->second;
    unsigned int id1 = LAMP->InstToIdMap[ldStPair->first];
    unsigned int id2 = LAMP->InstToIdMap[ldStPair->second];
    DEBUG(dbgs() << "Instruction " << "(" << id1 << ")\n" );
    DEBUG(dbgs() << "\t" << *(ldStPair->first) << "\n");
    DEBUG(dbgs() << " is dependent on " << "(" << id2 << ")\n");
    DEBUG(dbgs() << "\t" << *(ldStPair->second) << "\n");
    DEBUG(dbgs() << " for " << times << " time\n");
  }
  /*
  LoopToDepSetMap_t::iterator iter1;
  for(iter1 = LAMP->LoopToDepSetMap.begin(); iter1 != LAMP->LoopToDepSetMap.end(); iter1++){
    BasicBlock* headerBB = iter1->first; 
    DEBUG(dbgs() << "The basic block " << *headerBB << " in \n" );
        //<< (headerBB->getParent())->getName() << " has following dep set map:\n");
  }
  DEBUG(dbgs() << "IdToInstMap size:" << LAMP->IdToInstMap.size() << "\n");
  DEBUG(dbgs() << "InstToIdMap size:" << LAMP->InstToIdMap.size() << "\n");
  DEBUG(dbgs() << "IdToLoopMap size:" << LAMP->IdToLoopMap.size() << "\n");
  DEBUG(dbgs() << "LoopToIdMap size:" << LAMP->LoopToIdMap.size() << "\n");
  DEBUG(dbgs() << "DepToTimesMap size:" << LAMP->DepToTimesMap.size() << "\n");
  DEBUG(dbgs() << "LoopToDepSetMap size:" << LAMP->LoopToDepSetMap.size() << "\n");
  DEBUG(dbgs() << "LoopToMaxDepTimesMap size:" << LAMP->LoopToMaxDepTimesMap.size() << "\n");
  DEBUG(dbgs() << "idToSilentCount size:" << LAMP->idToSilentCount.size() << "\n");

  //for(Module::iterator I = M.begin(), E = M.end(); I != E; ++I){
  //  if(!I->isDeclaration()){
  //    Function &Func = *I;
  //    DEBUG(dbgs() << "Temporary:" << Func.getName() << "\n");
  //  }
  //}
  //For every load print what fraction of time its dependent on 
  //a store.
  for(Module::iterator I = M.begin(), E = M.end(); I != E; ++I){
    if(!I->isDeclaration()){
      Function &F = *I;
      DEBUG(dbgs() << "Processing:" << F.getName() << "\n");
      //PI->dump(&F);
      for(inst_iterator Iter = inst_begin(F), EF = inst_end(F); Iter != EF; ++Iter){
        Instruction &curInst = *Iter;
        if(isa<LoadInst>(&curInst)){
          double ldExeCount = PI->getExecutionCount(curInst.getParent());
          if(ldExeCount == ProfileInfo::MissingValue){
            DEBUG(dbgs() << "!!!!!! Missing Value !!!!!!\n");
          }
          DEBUG(dbgs() << "load exe count:" << (unsigned int)ldExeCount << "\n");
          DEBUG(dbgs() << "Load " << curInst << " is dependent on\n");
          for(iter = LAMP->DepToTimesMap.begin(); iter != LAMP->DepToTimesMap.end(); iter++){
            DepPair_t *ldStPair = iter->first;
            unsigned int times = iter->second;
            unsigned int id1 = LAMP->InstToIdMap[ldStPair->first];
            unsigned int id2 = LAMP->InstToIdMap[ldStPair->second];
            if(&curInst == ldStPair->first){
              DEBUG(dbgs() << *ldStPair->second << " for " << times << " times\n");
              //DEBUG(dbgs() << *ldStPair->second << " for " << (double)times/ldExeCount << " times\n");
            }
          }
        }
      }
    }
  }*/

#undef DEBUG_TYPE
#define DEBUG_TYPE "iocalls"
  //The following code is to check the types of I/O calls made.
  for(Module::iterator I = M.begin(), E = M.end(); I != E; ++I){
    if(I->isDeclaration()){
      DEBUG(dbgs() << "Library call:" << I->getName() << "\n");
    }
  }
#undef DEBUG_TYPE
#define DEBUG_TYPE "iocalls"



  flag = true;
  return false;
}
#undef DEBUG_TYPE
#define DEBUG_TYPE "vpAnalysis"
//This pass is to test and analyze value profiling.
namespace {
  class vpAnalysis : public ModulePass{
    public:
      static char ID;
      static bool flag;
      static char const * resultFile;
      static unsigned long long invariantValues;
      ProfileInfo *PI;
      VPLoadProfile *VP;
      bool runOnModule(Module &M);
      virtual void getAnalysisUsage(AnalysisUsage &AU) const{
        AU.setPreservesAll();
        AU.addRequired<ProfileInfo>();
        AU.addRequired<VPLoadProfile>();
      }
      vpAnalysis(): ModulePass(ID){
      }
  };
}
char vpAnalysis::ID = 0;
bool vpAnalysis::flag = false;
char const * vpAnalysis::resultFile = "value_profiling.csv";
unsigned long long vpAnalysis::invariantValues = 0;

static RegisterPass<vpAnalysis>
R("vp-analysis", "test and analyze the frequently occuring values");

ModulePass *llvm::createvpAnalysis(){
  return new vpAnalysis();
}

bool vpAnalysis::runOnModule(Module &M){
  if(flag == true)
    return false;
  std::ofstream resultOut;
  resultOut.open(resultFile);
  PI = &getAnalysis<ProfileInfo>();
  VP = &getAnalysis<VPLoadProfile>();
  resultOut << "total,opcode,value1,count1,value2,count2,value3,count3,value4,count4" << "\n";
  std::map<unsigned int, Instruction*>::iterator iter1;
  for(iter1 = VP->IdToInstMap.begin(); iter1 != VP->IdToInstMap.end(); iter1++){
    unsigned int id = iter1->first;
    Instruction* cInst = iter1->second;
    valueCountMap_t::iterator valCountIter;
    DEBUG(dbgs() << "for instruction " << *cInst << "\n");
    DEBUG(dbgs() << "there are " << VP->IdToValueCount[id]->size() << " values\n");
    unsigned int execCount = PI->getExecutionCount(cInst->getParent());
    resultOut << execCount << ",";
    resultOut << cInst->getOpcodeName(cInst->getOpcode())<< ",";
    DEBUG(dbgs() << "total Executions: " << execCount << "\n");
    for(valCountIter = VP->IdToValueCount[id]->begin(); valCountIter != VP->IdToValueCount[id]->end();
        valCountIter++){
      int value = valCountIter->first;
      unsigned int count = valCountIter->second;
      double fractionOfTotal = (double)count/execCount;
      if(fractionOfTotal >= 0.99){
        //invariantValues += 1;
        invariantValues += execCount;
      }
      resultOut << value << ",";
      resultOut << std::setprecision(3) << (double)count/execCount << ",";
      DEBUG(dbgs() << "\tthe value " << value << " occured " << (double)count/execCount << " times\n");
    }
    resultOut << "\n";
  }
  /*
  for(Module::iterator I = M.begin(), E = M.end(); I != E; ++I){
    if(!I->isDeclaration()){
      Function &F = *I;
      DEBUG(dbgs() << "Processing:" << F.getName() << "\n");
      //PI->dump(&F);
    }
  }*/
  if(localStats){
    errs() << invariantValues << " Total invariant values\n";
  }
  flag = true;
  resultOut.close();
  return false;
}
#undef DEBUG_TYPE
#define DEBUG_TYPE "getCalls"
//This pass is to get all the library calls.
namespace {
  class getCalls : public ModulePass{
    public:
      static char ID;
      static bool flag;
      std::set<Function*> libCalls;
      bool runOnModule(Module &M);
      virtual void getAnalysisUsage(AnalysisUsage &AU) const{
        AU.setPreservesAll();
      }
      getCalls(): ModulePass(ID){
      }
  };
}
char getCalls::ID = 0;
bool getCalls::flag = false;

static RegisterPass<getCalls>
T("get-calls", "get the library calls");

ModulePass *llvm::creategetCalls(){
  return new getCalls();
}

bool getCalls::runOnModule(Module &M){
  if(flag == true)
    return false;
  for(Module::iterator I = M.begin(), E = M.end(); I != E; ++I){
    if(I->isDeclaration()){
      //currently only insert printf
      if(I->getName() == "printf"){
        libCalls.insert(I);
        DEBUG(dbgs() << "Library call:" << I->getName() << "\n");
      }
    }
  }
  flag = true;
  return false;
}
#undef DEBUG_TYPE
#define DEBUG_TYPE "trueDup"
//This pass is to duplicate code based on lib calls and memory dependences.
namespace {
  typedef std::map<Instruction*, instrInfo*> tInstToInfoObj;
  class trueDup: public FunctionPass{
      bool runOnFunction(Function &F);
      Instruction* duplicateProdRec(Instruction *pInst);
      std::set<CallSite*> libCalls;
      tInstToInfoObj instToInfoObj;
      ProfileInfo *PI;
      LAMPLoadProfile *LAMP;
    public:
      static char ID;
      getCalls *CL;
      virtual void getAnalysisUsage(AnalysisUsage &AU) const{
        AU.setPreservesAll();
        AU.addRequired<getCalls>();
        if(TDUseLAMP){
          AU.addRequired<LAMPLoadProfile>();
        }
        if(TDUseLAMP){
          AU.addRequired<ProfileInfo>();
        }
      }
      std::set<Instruction*> getDependentStores(Instruction* ldInst);
      trueDup(): FunctionPass(ID){
        CL = NULL;
      }
  };
}
char trueDup::ID = 0;

static RegisterPass<trueDup>
S("true-dup", "get the library calls");

FunctionPass *llvm::createtrueDup(){
  return new trueDup();
}

bool trueDup::runOnFunction(Function &F){
  CL = &getAnalysis<getCalls>();
  if(TDUseLAMP){
    LAMP = &getAnalysis<LAMPLoadProfile>();
  }
  if(TDUseProfile){
    PI = &getAnalysis<ProfileInfo>();
  }
  DEBUG(dbgs() << "running on " << F.getName() << "\n");

  //clear the set
  libCalls.clear();
  for(tInstToInfoObj::iterator iter = instToInfoObj.begin();
      iter != instToInfoObj.end(); iter++){
      delete ((*iter).second);
  }
  instToInfoObj.clear();

  for(inst_iterator Iter = inst_begin(F), EF = inst_end(F); Iter != EF; ++Iter){
    Instruction* pInst = &*Iter;
    //popluate the bookkeeping object
    instrInfo* infoObj = new instrInfo(pInst);
    instToInfoObj[pInst] = infoObj;
    switch(pInst->getOpcode()){
      case Instruction::Call:
      case Instruction::Invoke:{
        CallSite* CS = new CallSite(pInst);
        Function* libCall = CS->getCalledFunction();
        if(libCall->getName() == "printf"){
          libCalls.insert(CS);
        }
        break;
      }
      default:
        break;
    }
  }
  DEBUG(dbgs() << "Number of lib calls:" << libCalls.size() << "\n");
  std::set<CallSite *>::iterator libcall;
  for(libcall = libCalls.begin(); libcall != libCalls.end(); ++libcall){
    CallSite* CS = *libcall;
    //iterate over arguments
    CallSite::arg_iterator B = CS->arg_begin(), E = CS->arg_end();
    for(CallSite::arg_iterator A = B; A != E; ++A){
      Value *val = A->get();
      if(isa<Constant>(val) || isa<Argument>(val) || isa<GlobalValue>(val)){
        DEBUG(dbgs() << " constant or arg or globalVal: " << val->getName() << "\n");
      }
      else{
        //it's an instruction otherwise 
        if(Instruction *usedInstr = dyn_cast<Instruction>(val)){
          duplicateProdRec(usedInstr);
        }
       DEBUG(dbgs() << *A->get() << "\n");
      }
    
    }
  }
  return false;
}
#undef DEBUG_TYPE
#define DEBUG_TYPE "dupRec"
Instruction* trueDup::duplicateProdRec(Instruction *pInst){
  Instruction* duplicatedInst = NULL;
  if(instToInfoObj[pInst]->isDuplicated){
    //duplicatedInst = instrVisited[pInst]->duplicatedBy;
  }
  else{
    if((!instToInfoObj[pInst]->isSafe)){
      if(pInst->getOpcode() == Instruction::Load){
        //encounter a load
        DEBUG(dbgs() << "load" << *pInst << "\n");
        DEBUG(dbgs() << "dependent on" << "\n");
        getDependentStores(pInst);
      }
      else if (pInst->getOpcode() == Instruction::Alloca || 
               pInst->getOpcode() == Instruction::Call){
      }
      else{
        instToInfoObj[pInst]->isDuplicated = true;
        //clone here
        //instrVisited[pInst]->duplicatedBy = dupInst;
        for(User::op_iterator itr =  pInst->op_begin(), 
              itr_e = pInst->op_end(); itr != itr_e; ++itr){
          Value* usedVal = *itr;
          if(Instruction *usedInstr = dyn_cast<Instruction>(usedVal)){
            duplicateProdRec(usedInstr);
          }
        }
      }
    }
  }
  return pInst;
}
std::set<Instruction*> trueDup::getDependentStores(Instruction* ldInst){
    std::set<Instruction*> worklist;
    if(isa<LoadInst>(ldInst) && TDUseLAMP){
      DepToTimesMap_t::iterator iter;
      double ldExeCount = PI->getExecutionCount(ldInst->getParent());
      for(iter = LAMP->DepToTimesMap.begin(); iter != LAMP->DepToTimesMap.end(); iter++){
        DepPair_t *ldStPair = iter->first;
        unsigned int times = iter->second;
        unsigned int id1 = LAMP->InstToIdMap[ldStPair->first];
        unsigned int id2 = LAMP->InstToIdMap[ldStPair->second];
        if(ldInst == ldStPair->first){
          //only the stores which are in current function should be returned.
          //FIXME: How should the dependent stores in other functions should be handled.
          if((ldInst->getParent())->getParent() == ((ldStPair->first)->getParent())->getParent()){
            //DEBUG(dbgs() << *ldStPair->second << " for " << times << " times\n");
            if(ldExeCount > 0.0){
              DEBUG(dbgs() << *ldStPair->second << " for " << (double)times/ldExeCount << " times\n");
            }
          }
        }
      }
    }

  return worklist;
}
