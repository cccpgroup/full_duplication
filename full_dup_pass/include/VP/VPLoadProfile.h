#ifndef VPLOADPROFILE_H
#define VPLOADPROFILE_H

namespace llvm{
  
  class ModulePass;
  class FunctionPass;
  class LoopPass;

  /*LoopPass *createVPBuildLoopMapPass();*/
  ModulePass *createVPLoadProfilePass();
  typedef std::map<int, unsigned int> valueCountMap_t;
  class VPLoadProfile : public ModulePass {
  public:
    std::map<unsigned int, Instruction*> IdToInstMap;   // InstID -> Inst*
    std::map<Instruction*, unsigned int> InstToIdMap;   // Inst* -> InstID
    std::map<unsigned int, valueCountMap_t*> IdToValueCount;
    
    static unsigned int vp_id;
    static char ID;
    VPLoadProfile() : ModulePass (ID){
    }
    virtual bool runOnModule (Module &M);
    virtual void getAnalysisUsage(AnalysisUsage &AU) const;
  };
}
#endif
