//This file is currently not distributed under any license. Happy?
//
#ifndef LLVM_TRANSFORMS_VPINSTRUMENTATION_H
#define LLVM_TRANSFORMS_VPINSTRUMENTATION_H

#include "llvm/IR/DataLayout.h"
namespace llvm {

  class ModulePass;
  /*class FunctionPass;*/
  /*class LoopPass;*/

  //Count the number of instructions
  ModulePass* createInstCounter();

  FunctionPass *createVPProfilerPass();

  /*LoopPass *createLAMPLoopProfilerPass();*/

  ModulePass *createVPInitPass();

}

#endif
