#ifndef GLOBALSTORES_H 
#define GLOBALSTORES_H 


namespace llvm{
  class ModulePass;

  ModulePass *createglobalStoresPass();

  class globalStores : public ModulePass {
    public:
      static char ID;
      static bool flag;
      ProfileInfo *PI;
      LAMPLoadProfile *LAMP;
      std::set<Instruction*> globalStrs;
      bool runOnModule(Module &M);
      virtual void getAnalysisUsage(AnalysisUsage &AU) const{
        AU.setPreservesAll();
        AU.addRequired<LAMPLoadProfile>();
        AU.addRequired<ProfileInfo>();
      }
      globalStores(): ModulePass(ID){
      }
  };
}
#endif
