#ifndef INSTR_REL_INFO_H
#define INSTR_REL_INFO_H

#include "llvm/ADT/BitVector.h"
//#include "llvm/Target/TargetInstrInfo.h"
//#include "llvm/Target/TargetMachine.h"
//#include "llvm/Target/TargetInstrDesc.h"
#include "llvm/IR/Instruction.h"
#include "llvm/IR/Instructions.h"
//#include "llvm/Target/TargetRegisterInfo.h"
#include "llvm/Support/CommandLine.h"
#include <map>
#include <list>
#include <set>
#include "llvm/IR/User.h"

using namespace llvm;

struct InstrRelInfo; // forward declaration
class instrInfo; // forward declaration
/*class globalStores;*/


namespace llvm{
  class ModulePass;
  class FunctionPass;
  ModulePass *createvpAnalysis();
  ModulePass *createsilentCount();
  ModulePass *creategetCalls();
  FunctionPass *createtrueDup();
}
namespace llvm{
  class ModulePass;
  ModulePass *createlampAnalysis();
}



// command line arguments (defined in RelCodeGen)
extern cl::opt<float> RelLiftThreshold;
extern cl::opt<float> RelSafeThreshold;
extern cl::opt<float> RelIScale;
extern cl::opt<float> RelIScaleThreshold;
extern cl::opt<bool>  RelPrintDbg;

struct InstrRelInfo {
  typedef std::map<BasicBlock*, double> ExecProb;
  typedef std::map<BasicBlock*, ExecProb*> BBProfInfo;

  static std::set<Instruction*> consumerSet;
  static std::set<Instruction*> producerSet;
  static std::list<unsigned int> sympBuckets;

 // static MachineRegisterInfo *mri;
 // static const TargetRegisterInfo *tri;
  //static BitVector reservedRegs;

  // heuristic parameters
  static std::pair<unsigned int, double> safeSympThreshold;
  static std::pair<unsigned int, double> liftSympThreshold;
  static double indirectScalingFactor;
  static double indirectScalingThreshold;
  static double directScalingThreshold;

  Instruction *instr;
  unsigned long uniqueID;
  const char * name;
  int opcode;

  struct RelUseDetails {
    unsigned int dist;
    double prob;
    InstrRelInfo* iri;
  };

  typedef std::map<Instruction*, RelUseDetails*> UseInfo;
  std::map<BasicBlock*, UseInfo *> useBBs;

  std::vector<Instruction*> defs;
  std::map<unsigned int, double> sympMapBucketed;
  std::map<unsigned int, double> directSympMap;
  std::map<unsigned int, double> indirectSympMap;

  // counters
  int numMemConsumers;
  int numGlobalMemConsumers;
  int numCallConsumers;
  int dupChainSize;
  //How many instructions needs to be replicated including myself
  int producerChainLength;
  //Producer chain length with their execution frequencies
  unsigned long dynProdChainLength;

  // flags
  bool isProtected;
  bool duplicated;
  bool producerSetReady;
  bool consumerSetReady;
  bool isSafe;
  bool isFunctionArgument;
  bool lifted;
  bool directSympGenDone;
  bool indirectSympGenDone;
  bool debug1;
  Instruction* duplicatedBy;
  bool isSilentStore;
  bool loadAlreadyConsidered;
  bool consideredForInvariance; //Is the value already considered for invaraince count?
  //related to producerChainLength
  bool visitedForLength;

  InstrRelInfo(Instruction* pInstr);

  static void initStaticMembers();
  bool printInstr();
  static bool accessesMem(Instruction* mi);
  static bool accessesGlobalMem(Instruction* mi);
  static bool isStore(Instruction* mi);
  static bool isLoad(Instruction* mi);
  static bool isGlobalStore(Instruction* mi);
  static bool isCall(Instruction* mi);
  static bool isImplicitDependence(Instruction* pProd, Instruction* pConsumer);

  bool isHighValue();

  void addUse(std::map<Instruction*, InstrRelInfo*> &iriMap, Instruction* mi, unsigned int dist);
  void markSafe();
  void genUseList(std::map<Instruction*, InstrRelInfo*> &iriMap);

  //unsigned int findDistance(MachineInstr * pDefInst, MachineInstr *pUseInst);
  //int doFindDistance(MachineBasicBlock *pDefBB, MachineBasicBlock *pCurBB,
  //                   std::map<MachineBasicBlock*, int> &distMap);

  void processDirectConsumers(BBProfInfo &bbProf);
  void processIndirectConsumers(BBProfInfo &bbProf);
  void doProcessDirectConsumers(BBProfInfo &bbProf, BasicBlock* pBB, unsigned int dist, double scale, int depth);
  void doProcessIndirectConsumers(BBProfInfo &bbProf, BasicBlock* pBB, unsigned int dist, double scale);
  void doProcessZerothCase(BBProfInfo &bbProf, BasicBlock* pBB, unsigned int dist, double scale);
  void doProcessZerothICase(BBProfInfo &bbProf, BasicBlock* pBB, unsigned int dist, double scale);
  void populateSympBuckets();
  void doPopulateSympBuckets(std::map<unsigned int, double> &sympMap);

  std::string printSymptomProb();
  std::string printSymptomMap();
};

//The following is to track relevent info for doing the instruction duplication
//by tracing dependences through memory.
class instrInfo{
  Instruction *instr;
  public:
    instrInfo(Instruction* cInstr);
    bool isHighValue();
    bool isDuplicated;
    bool isSafe;
    Instruction* duplicatedBy;
};

#endif
