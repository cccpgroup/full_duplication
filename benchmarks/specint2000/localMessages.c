#include <stdio.h>
void printValueMsg(int id){
  fprintf(stderr, "VP:Reliability CMP VP failed in %d\n", id);
  fflush(stderr);
}

void printValueMsgD(int failure, char* name, int expected, int observed){
  if(failure << 31){
    fprintf(stderr, "VP:Reliability CMP failed in %s\n", name);
    fprintf(stderr, "failure is %d!\n", failure);
    fprintf(stderr, "expected: %d, observed: %d\n", expected, observed);
    //exit(1);
    fflush(stderr);
  }
}
void printRelExitMsgD(int failure, char* name, int expected, int observed){
  if(failure << 31){
    fprintf(stderr, "VP:Reliability CMP failed in %s\n", name);
    fprintf(stderr, "failure is %d!\n", failure);
    fprintf(stderr, "expected: %d, observed: %d\n", expected, observed);
    //exit(1);
    fflush(stderr);
  }
}
void printFinalValueMsg(int messages){
    fprintf(stderr, "VP:Reliability CMP VP failed: %d\n", messages);
}
