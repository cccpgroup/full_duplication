#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "util.h"
#include "pr.h"
#include "ext.h"
#include "place.h"
#include "route.h"
#include "draw.h"
#include "stats.h"
#include "check_route.h"
#include "rr_graph.h"
#include "read_place.h"


extern int binary_search_place_and_route (struct s_placer_opts
      placer_opts, char *place_file, char *net_file, char *arch_file,
      char *route_file, boolean full_stats, boolean verify_binary_search,
      struct s_annealing_sched annealing_sched, struct s_router_opts
      router_opts, struct s_det_routing_arch det_routing_arch, struct 
      s_segment_inf *segment_inf, t_timing_inf timing_inf); 

/* void try_place (struct s_placer_opts placer_opts, 
  struct s_annealing_sched annealing_sched);*/
extern void read_place (char *place_file, char *net_file, char *arch_file,
            struct s_placer_opts placer_opts); 

extern void alloc_and_load_unique_pin_list (void);
extern void free_unique_pin_list (void); 
extern void alloc_place_regions (int num_regions);
extern void load_place_regions (int num_regions);
extern void free_place_regions (int num_regions); 
extern void alloc_and_load_placement_structs (int place_cost_type, 
       int num_regions, float place_cost_exp, float ***old_region_occ_x, 
       float ***old_region_occ_y); 
extern void free_placement_structs (int place_cost_type, int num_regions,
        float **old_region_occ_x, float **old_region_occ_y); 
extern void alloc_and_load_for_fast_cost_update (float place_cost_exp);

extern void initial_placement (enum e_pad_loc_type pad_loc_type,
            char *pad_loc_file);
extern float comp_cost (int method, int place_cost_type, int num_regions);
extern int try_swap (float t, float *cost, float rlim, int *pins_on_block,
       int place_cost_type, float **old_region_occ_x, 
       float **old_region_occ_y, int num_regions, boolean fixed_pins);
extern void check_place (float cost, int place_cost_type, int num_regions);
extern float starting_t (float *cost_ptr, int *pins_on_block, 
       int place_cost_type, float **old_region_occ_x, float **old_region_occ_y,
       int num_regions, boolean fixed_pins, struct s_annealing_sched 
       annealing_sched, int max_moves);
extern void update_t (float *t, float std_dev, float rlim, float success_rat,
       struct s_annealing_sched annealing_sched); 
extern void update_rlim (float *rlim, float success_rat);
extern int exit_crit (float t, float cost, struct s_annealing_sched
       annealing_sched);
extern double get_std_dev (int n, double sum_x_squared, double av_x);
extern void free_fast_cost_update_structs (void); 
extern float recompute_cost (int place_cost_type, int num_regions);
extern int assess_swap (float delta_c, float t);
extern void find_to (int x_from, int y_from, int type, float rlim, 
         int *x_to, int *y_to);
extern void update_bb (int inet, struct s_bb *bb_coord_new, struct s_bb 
        *bb_edge_new, int xold, int yold, int xnew, int ynew);
extern int find_affected_nets (int *nets_to_update, int *net_block_moved, 
        int b_from, int b_to, int num_of_pins);
extern float net_cost (int inet, struct s_bb *bb_ptr);
extern float nonlinear_cong_cost (int num_regions);
extern void update_region_occ (int inet, struct s_bb*coords, 
        int add_or_sub, int num_regions);
extern void save_region_occ (float **old_region_occ_x, 
        float **old_region_occ_y, int num_regions);
extern void restore_region_occ (float **old_region_occ_x,
        float **old_region_occ_y, int num_regions);
extern void get_bb_from_scratch (int inet, struct s_bb *coords, 
        struct s_bb *num_on_edges); 
extern float comp_width (struct s_chan *chan, float x, float separation);


void try_place (struct s_placer_opts placer_opts, 
        struct s_annealing_sched annealing_sched) {

/* Does almost all the work of placing a circuit.  Width_fac gives the   *
 * width of the widest channel.  Place_cost_exp says what exponent the   *
 * width should be taken to when calculating costs.  This allows a       *
 * greater bias for anisotropic architectures.  Place_cost_type          *
 * determines which cost function is used.  num_regions is used only     *
 * the place_cost_type is NONLINEAR_CONG.                                */

 int tot_iter, inner_iter, success_sum, pins_on_block[3];
 int move_lim, i, moves_since_cost_recompute, width_fac;
 float t, cost, success_rat, rlim, new_cost;
 float oldt;
 double av_cost, sum_of_squares, std_dev;
 float **old_region_occ_x, **old_region_occ_y;
 char msg[BUFSIZE];
 boolean fixed_pins;  /* Can pads move or not? */


 width_fac = placer_opts.place_chan_width;
 if (placer_opts.pad_loc_type == FREE)
    fixed_pins = FALSE;
 else
    fixed_pins = TRUE;

 init_chan(width_fac); 

 for (i=0;i<num_nets;i++)
    net[i].tempcost = -1.; /* Used to store costs for moves not yet made. *
                            * and to indicate when a net's cost has been  *
                            * recomputed.                                 */
  
 alloc_and_load_placement_structs (placer_opts.place_cost_type, 
            placer_opts.num_regions, placer_opts.place_cost_exp,
            &old_region_occ_x, &old_region_occ_y); 

 initial_placement (placer_opts.pad_loc_type, placer_opts.pad_loc_file);
 init_draw_coords ((float) width_fac);

/* Storing the number of pins on each type of block makes the swap routine *
 * slightly more efficient.                                                */

 pins_on_block[CLB] = pins_per_clb;
 pins_on_block[OUTPAD] = 1;
 pins_on_block[INPAD] = 1;

/* Gets initial cost and loads bounding boxes. */

 cost = comp_cost (NORMAL, placer_opts.place_cost_type, 
               placer_opts.num_regions); 

 move_lim = (int) (annealing_sched.inner_num * pow(num_blocks,1.3333));

/* Sometimes I want to run the router with a random placement.  Avoid *
 * using 0 moves to stop division by 0 and 0 length vector problems,  *
 * by setting move_lim to 1 (which is still too small to do any       *
 * significant optimization).                                         */

 if (move_lim <= 0) 
    move_lim = 1;  

 rlim = (float) max (nx, ny);
 t = starting_t (&cost, pins_on_block, placer_opts.place_cost_type,
             old_region_occ_x, old_region_occ_y, placer_opts.num_regions, 
             fixed_pins, annealing_sched, move_lim);
 tot_iter = 0;
 moves_since_cost_recompute = 0;

#ifdef SPEC_CPU2000
 fprintf(cost_fp, "Initial placement cost = %g\n\n",cost);
#else
 printf("Initial placement cost = %g\n\n",cost);
 printf("%11s  %10s  %7s  %7s  %7s  %10s  %7s\n", "T", "Av. Cost", "Ac Rate",
        "Std Dev", "R limit", "Tot. Moves", "Alpha");
 printf("%11s  %10s  %7s  %7s  %7s  %10s  %7s\n", "--------", "--------", 
        "-------", "-------", "-------", "----------", "-----");
#endif

 sprintf(msg,"Initial Placement.  Cost: %g.  Channel Factor: %d",
   cost, width_fac);
 update_screen(MAJOR, msg, PLACEMENT);

 while (exit_crit(t, cost, annealing_sched) == 0) {
    av_cost = 0.;
    sum_of_squares = 0.;
    success_sum = 0;

    for (inner_iter=0; inner_iter < move_lim; inner_iter++) {
       if (try_swap(t, &cost, rlim, pins_on_block, placer_opts.place_cost_type,
             old_region_occ_x, old_region_occ_y, placer_opts.num_regions,
             fixed_pins) == 1) {
          success_sum++;
          av_cost += cost;
          sum_of_squares += cost * cost;
       }
#ifdef VERBOSE
       printf("t = %f  cost = %f   move = %d\n",t, cost, inner_iter);
       if (fabs(cost - comp_cost(CHECK, placer_opts.place_cost_type, 
             placer_opts.num_regions)) > cost * ERROR_TOL) 
          exit(1);
#endif 
    }

/* Lines below prevent too much round-off error from accumulating *
 * in the cost over many iterations.  This round-off can lead to  *
 * error checks failing because the cost is different from what   *
 * you get when you recompute from scratch.                       */
 
    moves_since_cost_recompute += move_lim;
    if (moves_since_cost_recompute > MAX_MOVES_BEFORE_RECOMPUTE) {
       new_cost = recompute_cost (placer_opts.place_cost_type, 
                     placer_opts.num_regions);
       if (fabs(new_cost - cost) > cost * ERROR_TOL) {
          printf("Error in try_place:  new_cost = %f, old cost = %f.\n",
              new_cost, cost);
          exit (1);
       }
    
       cost = new_cost;
       moves_since_cost_recompute = 0;
    }

    tot_iter += move_lim;
    success_rat = ((float) success_sum)/ move_lim;
    if (success_sum == 0) {
       av_cost = cost;
    }
    else {
       av_cost /= success_sum;
    }
    std_dev = get_std_dev (success_sum, sum_of_squares, av_cost);

#ifndef SPEC_CPU2000
    printf("%11.5g  %10.6g  %7.4g  %7.3g  %7.4g  %10d  ",t, av_cost, 
          success_rat, std_dev, rlim, tot_iter);
#endif

    oldt = t;  /* for finding and printing alpha. */
    update_t (&t, std_dev, rlim, success_rat, annealing_sched);

#ifndef SPEC_CPU2000
    printf("%7.4g\n",t/oldt);
#endif

    sprintf(msg,"Cost: %g.  Temperature: %g",cost,t);
    update_screen(MINOR, msg, PLACEMENT);
    update_rlim (&rlim, success_rat);

#ifdef VERBOSE 
 dump_clbs();
#endif
 }

 t = 0;   /* freeze out */
 av_cost = 0.;
 sum_of_squares = 0.;
 success_sum = 0;

 for (inner_iter=0; inner_iter < move_lim; inner_iter++) {
    if (try_swap(t, &cost, rlim, pins_on_block, placer_opts.place_cost_type, 
          old_region_occ_x, old_region_occ_y, placer_opts.num_regions,
          fixed_pins) == 1) {
       success_sum++;
       av_cost += cost;
       sum_of_squares += cost * cost;
    }
#ifdef VERBOSE 
       printf("t = %f  cost = %f   move = %d\n",t, cost, tot_iter);
#endif
 }
 tot_iter += move_lim;
 success_rat = ((float) success_sum) / move_lim;
 if (success_sum == 0) {
    av_cost = cost;
 }
 else {
    av_cost /= success_sum;
 }

 std_dev = get_std_dev (success_sum, sum_of_squares, av_cost);

#ifndef SPEC_CPU2000
 printf("%11.5g  %10.6g  %7.4g  %7.3g  %7.4g  %10d \n\n",t, av_cost, 
       success_rat, std_dev, rlim, tot_iter);
#endif

#ifdef VERBOSE 
 dump_clbs();
#endif

 check_place(cost, placer_opts.place_cost_type, placer_opts.num_regions);
 sprintf(msg,"Final Placement.  Cost: %g.  Channel Factor: %d",cost,
    width_fac);
#ifdef SPEC_CPU2000
 fprintf(cost_fp, "Final Placement cost: %g\n",cost);
#else
 printf("Final Placement cost: %g\n",cost);
#endif
 update_screen(MAJOR, msg, PLACEMENT);

#ifdef SPEC_CPU2000
 printf ("Total moves attempted: %d.0\n", tot_iter);
#endif

 free_placement_structs (placer_opts.place_cost_type, placer_opts.num_regions,
        old_region_occ_x, old_region_occ_y); 
}
