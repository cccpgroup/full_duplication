#include <stdio.h>

int main(int argc, char* argv[]){
  if(argc > 1){
    printf("hello world with args\n");
  }
  else{
    printf("hello world w/o args\n");
  }
  return 0;
}
