#!/bin/csh -f

clang -emit-llvm -c main.c -o main.bc

opt  -insert-edge-profiling main.bc -o main.prof.bc

llc main.prof.bc -o main.prof.s
clang -static main.prof.s -o main.prof

./main.prof

opt -load /home/dskhudia/projects/full_duplication/full_dup_pass_build/lib/LLVMDuplicate.so -sample main.bc -o main.pi.bc

llvm-dis main.pi.bc

llc main.pi.bc -o main.pi.s
clang -static main.pi.s -o main.pi

./main.pi
./main.pi 5


