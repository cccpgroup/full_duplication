# where the benchmark tools are located
setenv BENCHTOOLS_ROOT      ${PROJECT_HOME}/benchmarks/benchtools
setenv DEFAULT_PROJECT      project 

# define host compiler
setenv HOST_COMPILER        gcc

# where the benchmarks are located
setenv USER_BENCH_PATH1     ${PROJECT_HOME}/benchmarks/specint2000
setenv USER_BENCH_PATH2     ${PROJECT_HOME}/benchmarks/mibench/automotive
setenv USER_BENCH_PATH3     ${PROJECT_HOME}/benchmarks/mibench/consumer
setenv USER_BENCH_PATH4

# find out host platform
if (`uname -m` == "ia64") then 
  setenv HOST_PLATFORM ia64lin
else if (`uname` == "Linux") then 
  setenv HOST_PLATFORM x86lin
else if (`uname` == "HP-UX") then
  setenv HOST_PLATFORM hp
else if (`uname` == "SunOS") then
  setenv HOST_PLATFORM sunsol
else if ("CYGWIN_NT-5.0" == `uname`) then
  setenv HOST_PLATFORM x86win
endif

# update path
set path = (${BENCHTOOLS_ROOT}/scripts $path)
