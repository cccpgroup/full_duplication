To Perform Code Duplication
=====

This repository contains [LLVM 3.6.2][llvm] source code, LLVM passes to perform
different type of profiling (e.g., value profiling) and code duplication. 

The current version of LLVM does not contain edge profiling infrastructure. Edge
profiling existed till the version 3.3 and the passes that I have developed require edge
profiling infrastructure. So I have merged edge profiling infrastructure from
3.3 to 3.6.2 and 3.6.2 version contained in this repository has edge profiling.

[llvm]: http://llvm.org/releases

Building LLVM 
-------------
Any method given on the LLVM website can be used to build and install LLVM. In
particular, you can follow following instructions to build LLVM.

* LLVM 3.6.2 requires gcc-4.7 or higher. Please make sure gcc-4.7 is installed
  as standard or CC and CXX environment variables are set to gcc-4.7 and
  g++-4.7, respectively. 
* Make a project directory (e.g., `~/full_duplication`). Let us say this directory name is stored in the environment variable `PROJECT_HOME`
* Execute following commands to build LLVM and the pass in the project directory

    cd ${PROJECT_HOME}  
    mkdir llvm_build  
    mkdir llvm_install  
    cd llvm_build  
    cmake ../llvm-3.6.2.src  
    cmake --build .  
    cmake -DCMAKE_INSTALL_PREFIX=${PROJECT_HOME}/llvm_install -P cmake_install.cmake  

Building Duplication Pass
-------------------------

* Execute following commands in the project directory to build the duplication pass 

    mkdir full_dup_pass_build  
    cd full_dup_pass_build  
    cmake -DLLVM_ROOT=${PROJECT_HOME}/llvm_build ../full_dup_pass  
    cmake --build .  
    cmake -DCMAKE_INSTALL_PREFIX=${PROJECT_HOME}/llvm_install -P cmake_install.cmake  
    cd ..  
    cd profiling_hooks  
    make  


Running Benchmarks
------------------
* Change to benchmarks directory, and in the source.me file change the value of
  `PROJECT_HOME` variable so that it points to the correct project path. If you use
  CSH, source this file. For any other shell, set the environment given in this file.
  
* To compile a spec benchmark, change to specint2000 directory. (To see what commands are executed for a particular option, run the make commands with -n option)

    ```make TESTCASE=164.gzip runclean``` : build benchmark without duplication and run.  
    ```make TESTCASE=164.gzip runrel```: build benchmark and run with full duplication. Do not use profiling.  

* By default, it does not do profiling and the duplication pass does not use
  profiling to optimize duplication. Profiling can be enabled by the following
  commands.

     ```make OPT_TYPE=2 TESTCASE=164.gzip runclean```  
     ```make OPT_TYPE=2 TESTCASE=164.gzip runrel```  


References:
-----------
The process and terminology used in developing the LLVM passes is described
  in the following papers.

[Efficient Soft Error Protection for Commodity Embedded Microprocessors using
  Profile Information][profiling]

[Shoestring: Probabilistic Soft Error Reliability on the Cheap][shoestring]

[profiling]:  http://cccp.eecs.umich.edu/papers/dskhudia-lctes12.pdf
[shoestring]: http://cccp.eecs.umich.edu/papers/sfeng-asplos10.pdf
